module protocol-database

go 1.15

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/aymerick/douceur v0.2.0
	github.com/beevik/etree v1.1.0
	github.com/go-chi/chi v3.3.2+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/css v1.0.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/rs/cors v1.7.0
	github.com/vektah/gqlparser/v2 v2.1.0
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b
	golang.org/x/tools v0.0.0-20200114235610-7ae403b6b589
)
