package protocol_scanner

import (
	"strings"
)

type ChapterScanner struct {
	currentChapter Chapter
	nextLineIsParagraphStart bool
	lastLineWasTitle bool
	currentTitleTextAttr TextAttributes
	currentTileBold bool
	chapters []Chapter
}

func NewChapterScanner() *ChapterScanner {
	return &ChapterScanner{
		currentChapter: Chapter{},
		nextLineIsParagraphStart: true,
		lastLineWasTitle: false,
		currentTitleTextAttr: TextAttributes{},
		currentTileBold: true,
		chapters: []Chapter{},
	}
}

func couldBeTitleLine(line* DocumentLine) bool {
	if line.Text() == "" {
		return false
	}
	if len(line.TestSpans) != 1 {
		return false
	}
	return line.TestSpans[0].Bold
}

func (cs* ChapterScanner) StartNewParagraph() {
	cs.nextLineIsParagraphStart = true
}

func (cs*ChapterScanner) ProcessLine(line *DocumentLine) {
	if cs.currentChapter.Headline == "" && line.Text() != "" {
		cs.currentChapter.Headline = line.Text()
		cs.currentChapter.Range = line.Range
		cs.currentTileBold = line.TestSpans[0].Bold
		cs.currentTitleTextAttr = line.TestSpans[0].Attr
		cs.lastLineWasTitle = true
		cs.nextLineIsParagraphStart = false
		return
	}
	if cs.nextLineIsParagraphStart && couldBeTitleLine(line) {
		// This is a title line, and its the first title line!
		cs.CloseChapter()
		cs.currentChapter.Headline = line.Text()
		cs.currentChapter.Range = line.Range
		cs.currentTileBold = line.TestSpans[0].Bold
		cs.currentTitleTextAttr = line.TestSpans[0].Attr
		cs.lastLineWasTitle = true
		cs.nextLineIsParagraphStart = false
		return
	}
	if cs.lastLineWasTitle &&
			len(line.TestSpans) == 1 &&
			line.TestSpans[0].Attr == cs.currentTitleTextAttr &&
			line.TestSpans[0].Bold == cs.currentTileBold {
		// This is a title-line, and it continues the last one
		cs.currentChapter.Headline += " " + line.Text()
		cs.currentChapter.Range = cs.currentChapter.Range.Merge(line.Range)
		cs.lastLineWasTitle = true
		cs.nextLineIsParagraphStart = false
		return
	}
	// At this point, we know it cannot be a title anymore
	if cs.nextLineIsParagraphStart {
		if int(line.Range.Start.Position - cs.currentChapter.Range.End.Position) > line.TestSpans[0].Attr.FontSizeOrLineHeight() {
			cs.currentChapter.Text += "\n"
		}
		cs.currentChapter.Text += "\n" + line.Text()
	} else {
		cs.currentChapter.Text += " " + line.Text()
	}
	cs.lastLineWasTitle = false
	cs.nextLineIsParagraphStart = false

	if cs.currentChapter.Range.Start.Page == 0 {
		cs.currentChapter.Range = line.Range
	} else {
		cs.currentChapter.Range = cs.currentChapter.Range.Merge(line.Range)
	}
	return
}

func (cs* ChapterScanner) CloseChapter() {
	if cs.currentChapter.Headline == "" && cs.currentChapter.Text == "" {
		return
	}
	// Remove double whitespaces
	cs.currentChapter.Text = strings.Trim(strings.ReplaceAll(cs.currentChapter.Text, "  ", " "), " \n")
	cs.currentChapter.Headline = strings.Trim(strings.ReplaceAll(cs.currentChapter.Headline, "  ", " "), " \n")
	cs.chapters = append(cs.chapters, cs.currentChapter)
	cs.currentChapter = Chapter{}
}

func (cs* ChapterScanner) FinishDocument() {
	cs.CloseChapter()
}
