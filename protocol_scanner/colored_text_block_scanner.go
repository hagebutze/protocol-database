package protocol_scanner

type ColoredTextBlockScanner struct {
	hueMin int
	hueMax int
	lastWordWasPartOfBlock bool
	currentBlock *PositionedText
	blocks []PositionedText
}

func NewColoredBlockScanner(hueMin int, hueMax int) *ColoredTextBlockScanner {
	return &ColoredTextBlockScanner{
		hueMin: hueMin,
		hueMax: hueMax,
		lastWordWasPartOfBlock: false,
		currentBlock: nil,
		blocks: []PositionedText{},
	}
}

func (s*ColoredTextBlockScanner) ProcessTextSpan(span DocumentTextSpan) {
	if isSignificantAndInHueRange(s.hueMin, s.hueMax, span.Attr.Font.Color) {
		if s.lastWordWasPartOfBlock {
			s.currentBlock.Range = s.currentBlock.Range.Merge(span.Range)
			s.currentBlock.Text += " " + span.Text
		} else {
			s.lastWordWasPartOfBlock = true
			s.currentBlock = &PositionedText{
				Text: span.Text,
				Range: span.Range,
			}
		}
	} else {
		s.FinishBlock()
	}
}

func (s*ColoredTextBlockScanner) FinishBlock() {
	if !s.lastWordWasPartOfBlock {
		return
	}
	s.blocks = append(s.blocks, *s.currentBlock)
	s.currentBlock = nil
	s.lastWordWasPartOfBlock = false
}