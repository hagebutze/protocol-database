package protocol_scanner

import "time"

type DocumentPosition struct {
	Page int
	Position float64
}

func (dp* DocumentPosition) Before(other* DocumentPosition) bool {
	if dp.Page < other.Page {
		return true
	}
	if other.Page < dp.Page {
		return false
	}
	return dp.Position < other.Position
}

func (dp* DocumentPosition) After(other* DocumentPosition) bool {
	if dp.Page > other.Page {
		return true
	}
	if other.Page > dp.Page {
		return false
	}
	return dp.Position > other.Position
}

type DocumentRange struct {
	Start DocumentPosition
	End   DocumentPosition
}

func Min(a DocumentPosition, b DocumentPosition) DocumentPosition {
	if a.Page < b.Page {
		return a
	}
	if b.Page < a.Page {
		return b
	}
	if a.Position < b.Position {
		return a
	}
	return b
}

func Max(a DocumentPosition, b DocumentPosition) DocumentPosition {
	if a.Page > b.Page {
		return a
	}
	if b.Page > a.Page {
		return b
	}
	if a.Position > b.Position {
		return a
	}
	return b
}

func (a* DocumentRange) Merge(b DocumentRange) DocumentRange {
	return DocumentRange{
		Start: Min(a.Start, b.Start),
		End:   Max(a.End, b.End),
	}
}

type PositionedText struct {
	Text  string
	Range DocumentRange
}

type Chapter struct {
	Headline string
	Text string
	Range DocumentRange
}

type Consensus = PositionedText

type Decision = PositionedText

func IsInsideChapter(t *PositionedText, c *Chapter) bool {
	if c.Range.Start.After(&t.Range.End) ||  c.Range.End.Before(&t.Range.Start) {
		return false
	}
	return true
}

type Protocol struct {
	Date time.Time
	Type string
	Chapters    []Chapter
	Consensuses []Consensus
	Decisions   []Decision
}
