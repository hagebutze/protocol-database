package protocol_scanner

import (
	"errors"
	"fmt"
	"github.com/aymerick/douceur/parser"
	"golang.org/x/net/html"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

type DocumentTextElement struct {
	Range DocumentRange
}

// A word, in a pdf document. We make the assumption that every word has just 1 Font and color
type DocumentTextSpan struct {
	DocumentTextElement
	Attr TextAttributes
	Bold bool
	Text  string
}

type DocumentLine struct {
	DocumentTextElement
	TestSpans []DocumentTextSpan
}

func (l *DocumentLine) Text() string {
	result := ""
	for _, word := range l.TestSpans {
		if len(result) == 0 {
			result = word.Text
		} else {
			result += " " + word.Text
		}
	}
	return result
}

type DocumentParagraph struct {
	DocumentTextElement
	Lines []DocumentLine
}

type DocumentElementParseState struct {
	finishedParagraphs []DocumentParagraph
	currentParagraph   DocumentParagraph
	currentLine        DocumentLine
	nextTextSpanIsStartOfParagraph bool
}

type TextAttributesWithCount struct {
	Attr TextAttributes
	Count int
}

func (s* DocumentElementParseState) StartNewParagraph() {
	s.nextTextSpanIsStartOfParagraph = true
}

func (s* DocumentElementParseState) AddTextSpan(text string, attr TextAttributes, bold bool, page int, topPos int) {
	text = strings.Trim(text, " \n")
	if text == "" {
		return
	}
	newSpan := DocumentTextSpan{
		DocumentTextElement{
			Range: DocumentRange{
				Start: DocumentPosition{
					Page:     page,
					Position: float64(topPos),
				},
				End: DocumentPosition{
					Page:     page,
					Position: float64(topPos + attr.FontSizeOrLineHeight()),
				},
			},
		},
		attr,
		bold,
		text,
	}

	// Is it the first span in the line?
	if len(s.currentLine.TestSpans) == 0 {
		if s.nextTextSpanIsStartOfParagraph && len(s.currentParagraph.Lines) > 0 {
			s.FinishParagraph()
		}
		s.currentLine.Range = newSpan.Range
		s.currentLine.TestSpans = []DocumentTextSpan{newSpan}
	} else {
		// There is already a span in the line
		lastSpan := &s.currentLine.TestSpans[len(s.currentLine.TestSpans)-1]
		// Is the text span in line with the last span?
		if lastSpan.Range.Start.Page != newSpan.Range.Start.Page ||
			lastSpan.Range.End.Position < newSpan.Range.Start.Position {
			// The new text span forms a new line
			if s.nextTextSpanIsStartOfParagraph {
				s.FinishParagraph()
			}
			s.AddTextSpan(text, attr, bold, page, topPos)
		} else {
			// The new text span is in line with the current text span
			s.currentLine.Range = s.currentLine.Range.Merge(newSpan.Range)
			if lastSpan.Attr == newSpan.Attr && lastSpan.Bold == newSpan.Bold {
				lastSpan.Text += " " + newSpan.Text
			} else {
				s.currentLine.TestSpans = append(s.currentLine.TestSpans, newSpan)
			}
		}
	}
	// Its not the start of the paragraph anymore
	s.nextTextSpanIsStartOfParagraph = false
}

func (s*DocumentElementParseState) FinishLine() {
	if len(s.currentLine.TestSpans) == 0 {
		return // The line does not yet exist
	}

	if len(s.currentParagraph.Lines) == 0 {
		s.currentParagraph.Range = s.currentLine.Range
	} else {
		s.currentParagraph.Range = s.currentParagraph.Range.Merge(s.currentLine.Range)
	}
	s.currentParagraph.Lines = append(s.currentParagraph.Lines, s.currentLine)
	s.currentLine = DocumentLine{}
}
//
//func (s*PDFElementParseState) FinishBlockIfNewLineHasDistance() {
//	if len(s.currentBlock.Lines) > 0 {
//		lastLine := &s.currentBlock.Lines[len(s.currentBlock.Lines)-1]
//		// New page? than the block is finished
//		if lastLine.Range.End.Page != s.currentLine.Range.Start.Page {
//			s.FinishBlock()
//			return
//		}
//		// What about the distance of the lines?
//		if (s.currentLine.Range.Start.Position - lastLine.Range.End.Position) >
//			(s.currentLine.Range.End.Position - s.currentLine.Range.Start.Position) * 0.5 {
//			s.FinishBlock()
//			return
//		}
//	}
//}
//
func (s*DocumentElementParseState) FinishParagraph() {
	// Finish the current line first
	s.FinishLine()

	if len(s.currentParagraph.Lines) == 0 {
		return // The block does not yet exist
	}
	s.finishedParagraphs = append(s.finishedParagraphs, s.currentParagraph)
	s.currentParagraph = DocumentParagraph{}
}

func getPositionFromStyle(style string) (int, int, error) {
	declarations, err := parser.NewParser(style).ParseDeclarations()
	if err != nil {
		return 0, 0, err
	}
	top, left := 0, 0
	for _, declaration := range declarations {
		switch declaration.Property {
		case "top":
			top, err = parsePixelString(declaration.Value)
			if err != nil {
				return 0, 0, err
			}
		case "left":
			left, err = parsePixelString(declaration.Value)
			if err != nil {
				return 0, 0, err
			}
		}

	}
	return top, left, nil
}

func PDFToDocument(path string) ([]DocumentParagraph, error) {
	dir, err := ioutil.TempDir("", "pdftohtml")
	if err != nil {
		return nil, err
	}
	defer os.RemoveAll(dir)

	// Run pdftohtml
	_, err = exec.Command("pdftohtml", "-c", path, dir + "/out").Output()
	if err != nil {
		return nil, err
	}

	// List directory to know the number of pages
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}
	regex, err := regexp.Compile("out-[1-9][0-9]*\\.html")
	if err != nil {
		return nil, err
	}
	var pages []string
	for _, file := range files {
		if regex.MatchString(file.Name()) {
			pages = append(pages, file.Name())
		}
	}

	// Initialize parser
	parseState := DocumentElementParseState{}
	// Go through all pages
	for pageNum := 1; pageNum <= len(pages); pageNum++ {
		fmt.Printf("Scanning page %d\n", pageNum)

		file,err := os.Open(dir + "/" + pages[pageNum-1])
		if err != nil {
			return nil, err
		}
		page, err := html.Parse(file)
		if err != nil {
			return nil, err
		}
		// Get the root of the document
		root := getFirstChildNodeOfType(page, "html")
		if root == nil {
			return nil, errors.New("no root html tag found")
		}
		// Get the text attributes
		attributesMap, err := getFontsAsTextAttributes(root)
		if err != nil {
			return nil, err
		}
		// Scan the body!
		body := getFirstChildNodeOfType(root, "body")
		if root == nil {
			return nil, errors.New("no body html tag found")
		}
		bodyDiv := getFirstChildNodeOfType(body, "div")
		if root == nil {
			return nil, errors.New("no div html tag found in body")
		}
		// Go through all paragraphs
		for bodyChild := bodyDiv.FirstChild; bodyChild != nil; bodyChild = bodyChild.NextSibling {
			// Ignore all but paragraphs
			if bodyChild.Type != html.ElementNode || bodyChild.Data != "p" {
				continue
			}
			// Get the font type via the class
			class, found := getNodeAttribute(bodyChild, "class")
			if !found {
				return nil, errors.New("paragraph without class found")
			}
			// Get the text attribute
			textAttributes, found := attributesMap[class]
			if !found {
				return nil, errors.New(fmt.Sprintf("could not find class %s", class))
			}
			// Get the text position
			style, found := getNodeAttribute(bodyChild, "style")
			if !found {
				return nil, errors.New("paragraph without style found")
			}
			topPos, _, err := getPositionFromStyle(style)
			if err != nil {
				return nil, err
			}
			// Scan the paragraph!
			parseState.StartNewParagraph()
			err = scanParagraphData(&parseState, bodyChild, textAttributes, pageNum, topPos, false)
			if err != nil {
				return nil, err
			}
		}
	}
	parseState.FinishLine()
	parseState.FinishParagraph()
	return parseState.finishedParagraphs, nil
}

func scanParagraphData(parseState *DocumentElementParseState, p *html.Node, textAttributes TextAttributes, page int, topPos int, isBold bool) error {
	for pChild := p.FirstChild; pChild != nil; pChild = pChild.NextSibling {
		switch pChild.Type {
		case html.ElementNode:
			switch pChild.Data {
			case "b":
				err := scanParagraphData(parseState, pChild, textAttributes, page, topPos, true)
				if err != nil {
					return err
				}
			case "a":
				err := scanParagraphData(parseState, pChild, textAttributes, page, topPos, isBold)
				if err != nil {
					return err
				}
			case "br":
				parseState.FinishLine()
				topPos = topPos + textAttributes.FontSizeOrLineHeight()
			case "i":
				// Ignore images
			default:
				return errors.New(fmt.Sprintf("unexpected tag %s", pChild.Data))
			}
		case html.TextNode:
			parseState.AddTextSpan(strings.ReplaceAll(pChild.Data, string(rune(0xA0)), " "), textAttributes, isBold, page, topPos)
		default:
			return errors.New(fmt.Sprintf("unexpected node typ: %d", uint32(pChild.Type)))
		}
	}
	return nil
}
