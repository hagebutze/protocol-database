package protocol_scanner

import (
	"errors"
	"fmt"
	"github.com/aymerick/douceur/parser"
	"golang.org/x/net/html"
	"strconv"
)

func getNodeAttribute(node* html.Node, name string) (string, bool) {
	for _, a := range node.Attr {
		if a.Key == name {
			return a.Val, true
		}
	}
	return "", false
}

func getFirstChildNodeOfType(topNode* html.Node, t string) *html.Node {
	// Fo through all the child
	for child := topNode.FirstChild; child != nil; child = child.NextSibling {
		if child.Type == html.ElementNode && child.Data == t {
			return child
		}
	}
	return nil
}

func parsePixelString(s string) (int, error) {
	v, err := strconv.ParseInt(s[:len(s)-2], 10, 32)
	if err != nil {
		return 0, errors.New(fmt.Sprintf("error parsing pixel string %s: %v", s, err))
	}
	return int(v), nil
}

func getFontsAsTextAttributes(pageRoot *html.Node) (map[string]TextAttributes, error) {
	// Get the head and all the fonts
	head := getFirstChildNodeOfType(pageRoot, "head")
	if head == nil {
		return nil, errors.New("no head found in document")
	}
	// Get the first styles in the head
	fontStyle := getFirstChildNodeOfType(head, "style")
	if head == nil {
		return nil, errors.New("no font style found in document")
	}
	if fontStyle.FirstChild.Type != html.TextNode {
		return nil, errors.New("font style does not contain text node")
	}
	// Parse the css from the head
	stylesheet, err := parser.Parse(fontStyle.FirstChild.Data)
	if err != nil {
		return nil, err
	}
	// Put the result into the result
	result := map[string]TextAttributes{}
	for _, rule := range stylesheet.Rules {
		if len(rule.Selectors) == 1 && rule.Selectors[0] == "p" {
			continue // We ignore the "p" selector
		}
		if len(rule.Selectors) != 1 || rule.Selectors[0][0] != '.' {
			return nil, errors.New(fmt.Sprintf("i dont know how to parse these css selectors: %v",
				rule.Selectors))
		}

		// Create the text attributes
		attr := TextAttributes{}
		for _, declaration := range rule.Declarations {
			switch declaration.Property {
			case "font-size":
				fs, err := parsePixelString(declaration.Value)
				if err != nil {
					return nil, errors.New(fmt.Sprintf("error parsing font size: %v", err))
				}
				attr.Font.Size = int(fs)
			case "font-family":
				attr.Font.Name = declaration.Value
			case "color":
				attr.Font.Color, err = parseHexColor(declaration.Value)
				if err != nil {
					return nil, errors.New(fmt.Sprintf("error parsing color: %v", err))
				}
			}
		}
		result[rule.Selectors[0][1:]] = attr
	}
	return result, nil
}