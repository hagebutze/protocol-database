package protocol_scanner

import (
	"image/color"
)


type FontAttributes struct {
	Name string
	Size int
	Color color.Color
}

type TextAttributes struct {
	Font FontAttributes
	LineHeight int
}

func (ta* TextAttributes) FontSizeOrLineHeight() int {
	if ta.LineHeight > ta.Font.Size {
		return ta.LineHeight
	}
	return ta.Font.Size
}

type TextAttributeSet struct {
	Attributes []TextAttributesWithCount
	DominantAttribute*TextAttributesWithCount
}

func EqualAttrs(a TextAttributes, b TextAttributes) bool {
	return a.Font.Color == b.Font.Color &&
		a.Font.Name == b.Font.Name &&
		a.Font.Size == b.Font.Size
}

func (as* TextAttributeSet) AddAttributesWithCount(attr TextAttributesWithCount) {
	if len(as.Attributes) == 0 {
		// First attributes
		as.Attributes = append(as.Attributes, attr)
		as.DominantAttribute = &as.Attributes[0]
	} else {
		// Find the attributes, that match the attributes of the new text element
		currentAttrIndex := -1
		for i, a := range as.Attributes {
			if EqualAttrs(attr.Attr, a.Attr) {
				currentAttrIndex = i
				break
			}
		}
		if currentAttrIndex == -1 {
			// No attributes found, add a new one
			as.Attributes = append(as.Attributes, attr)
			currentAttrIndex = len(as.Attributes)-1
		} else {
			// Increase the count the of the correct attributes
			as.Attributes[currentAttrIndex].Count += attr.Count
		}
		// Update dominant
		if as.Attributes[currentAttrIndex].Count > as.DominantAttribute.Count {
			as.DominantAttribute = &as.Attributes[currentAttrIndex]
		}
	}
}

func (as* TextAttributeSet) AddAttributes(attr TextAttributes) {
	as.AddAttributesWithCount(
		TextAttributesWithCount{
			Attr:  attr,
			Count: 1,
		})
}

func (as* TextAttributeSet) AddSet(o* TextAttributeSet) {
	for _, other := range o.Attributes {
		as.AddAttributesWithCount(other)
	}
}
