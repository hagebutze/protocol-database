package protocol_scanner

import (
	"fmt"
	"image/color"
	"math"
)

func parseHexColor(s string) (c color.RGBA, err error) {
	c.A = 0xff
	switch len(s) {
	case 7:
		_, err = fmt.Sscanf(s, "#%02x%02x%02x", &c.R, &c.G, &c.B)
	case 4:
		_, err = fmt.Sscanf(s, "#%1x%1x%1x", &c.R, &c.G, &c.B)
		// Double the hex digits:
		c.R *= 17
		c.G *= 17
		c.B *= 17
	default:
		err = fmt.Errorf("invalid length, must be 7 or 4")

	}
	return
}

func getHueIfSignificant(color color.Color) (int, bool) {
	r, g, b, _ := color.RGBA()
	R := float64(r / 255.0)
	G := float64(g / 255.0)
	B := float64(b / 255.0)
	max := math.Max(R, math.Max(G, B))
	min := math.Min(R, math.Min(G, B))

	// Any hue significant hue value?
	if math.Abs(max-min) < 0.01 {
		return 0, false
	}

	var hue int
	if R == max {
		hue = int(math.Round((G - B) / (max - min) * 60.0))
	} else {
		if G == max {
			hue = int((2.0 + math.Round((B-R)/(max-min))) * 60.0)
		} else {
			hue = int((4.0 + math.Round((R-G)/(max-min))) * 60.0)
		}
	}
	if hue < 0 {
		return hue + 360, true
	}
	return hue, true
}

func hueInRange(hue int, hueMin int, hueMax int) bool {
	if hueMin < hueMax {
		return hue >= hueMin && hue <= hueMax
	} else {
		return hue <= hueMax || hue >= hueMin
	}
}

func isSignificantAndInHueRange(hueMin int, hueMax int, color color.Color) bool {
	hue, significant := getHueIfSignificant(color)
	if !significant {
		return false
	}
	return hueInRange(hue, hueMin, hueMax)
}