package protocol_scanner

import (
	"time"
)

func ScanProtocolPdf(path string, date time.Time) (*Protocol, error) {
	decisionScanner := NewColoredBlockScanner(345, 15)
	consensusScanner := NewColoredBlockScanner(80, 150)
	chapterScanner := NewChapterScanner()

	paragraphs, err := PDFToDocument(path)
	if err != nil {
		return nil, err
	}
	for _, paragraph := range paragraphs {
		chapterScanner.StartNewParagraph()
		for _, line := range paragraph.Lines {
			chapterScanner.ProcessLine(&line)
			for _, span := range line.TestSpans {
				decisionScanner.ProcessTextSpan(span)
				consensusScanner.ProcessTextSpan(span)
			}
		}
	}

	decisionScanner.FinishBlock()
	consensusScanner.FinishBlock()
	chapterScanner.FinishDocument()

	return &Protocol{
		Date: date,
		Type: "butzenplenum",
		Chapters: chapterScanner.chapters,
		Decisions: decisionScanner.blocks,
		Consensuses: consensusScanner.blocks,
	}, nil
}
