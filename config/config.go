package config

import (
	"encoding/json"
	"io/ioutil"
	"strings"
)

type Config struct {
	Webroot string `json:"webroot"`
	Database struct {
		Username string `json:"username"`
		Password string `json:"password"`
		Hostname string `json:"hostname"`
		Database string `json:"database"`
	} `json:"database"`
	Mattermost struct {
		Domain           string `json:"domain"`
		BotName          string `json:"bot_name"`
		Email            string `json:"email"`
		Password         string `json:"password"`
		Username         string `json:"username"`
		FirstName        string `json:"first_name"`
		LastName         string `json:"last_name"`
		TeamName         string `json:"team_name"`
		ChannelName      string `json:"channel_name"`
		DebugChannelName string `json:"debug_channel_name"`
	} `json:"mattermost"`
	NextCloud struct {
		Url                  string `json:"url"`
		Password             string `json:"password"`
		Username             string `json:"username"`
		ProtocolsPathPattern string `json:"protocols_path_pattern"`
	} `json:"nextcloud"`
	EMail struct {
		Smtp      string `json:"smtp"`
		SmtpPort  int    `json:"smtp_port"`
		Password  string `json:"password"`
		Username  string `json:"username"`
		Recipient string `json:"recipient"`
	} `json:"email"`
}

func LoadConfig(filename string) (*Config, error) {
	// The result object
	var config Config

	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(bytes, &config)
	if err != nil {
		return nil, err
	}

	config.Webroot = strings.Trim(config.Webroot, "/")
	if config.Webroot != "" {
		config.Webroot = "/" + config.Webroot
	}
	return &config, nil
}
