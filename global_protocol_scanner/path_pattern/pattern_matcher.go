package path_pattern

import (
	"protocol-database/nextcloud_file"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	patterns = map[string]Pattern{}

	monthNames = []string{
		"Januar",
		"Februar",
		"März",
		"April",
		"Mai",
		"Juni",
		"Juli",
		"August",
		"September",
		"Oktober",
		"November",
		"Dezember",
	}
)

type Pattern struct {
	regex         string
	matchingRegex string
}

func init() {
	patterns["{year}"] = Pattern{
		"([1-9][0-9]{3})",
		"(?P<year>[1-9][0-9]{3})",
	}
	patterns["{month}"] = Pattern{
		"([01][0-9])",
		"(?P<month>[01][0-9])",
	}
	patterns["{monthName}"] = Pattern{
		"(?:" + strings.Join(monthNames, "|") + ")",
		"",
	}
	patterns["{yearShort}"] = Pattern{
		"([1-9][0-9]{1})",
		"",
	}
	patterns["{day}"] = Pattern{
		"([0-3][0-9])",
		"(?P<day>[0-3][0-9])",
	}
	patterns["{ext}"] = Pattern{
		"(.*)",
		"",
	}
	patterns["{-}"] = Pattern {
		regex: "(-?)",
		matchingRegex: "",
	}
	patterns["{any}"] = Pattern{
		regex:         "(.*)",
		matchingRegex: "",
	}
}

type MatchedParameters struct {
	year  string
	month string
	day   string
	ext   string
}

type PathWalker struct {
	pattern            string
	matchedParameters  map[string]string
	file *nextcloud_file.NextcloudFile
	matchedPath        string
	remainingPathParts []string
	nextPartRegex      *regexp.Regexp
}

func NewPathWalker(pattern string, ext *string) PathWalker {
	result := PathWalker{
		pattern:            pattern,
		matchedPath:        "",
		matchedParameters:  map[string]string{},
		remainingPathParts: strings.Split(pattern, "/"),
	}
	if result.remainingPathParts[0] == "" {
		result.remainingPathParts = result.remainingPathParts[1:len(result.remainingPathParts)]
	}
	if ext != nil {
		result.matchedParameters["{ext}"] = *ext
	}
	result.reduceNonRegexSubPath()
	return result
}

func (pw *PathWalker) protocolPathPatternToRegex() (result *regexp.Regexp) {
	regexExpr := pw.remainingPathParts[0]
	for pattern, matchers := range patterns {
		if parameterValue, found := pw.matchedParameters[pattern]; found {
			regexExpr = strings.ReplaceAll(regexExpr, pattern, parameterValue)
		} else {
			if matchers.matchingRegex != "" {
				regexExpr = strings.Replace(regexExpr, pattern, matchers.matchingRegex, 1)
			}
			regexExpr = strings.ReplaceAll(regexExpr, pattern, matchers.regex)
		}
	}
	return regexp.MustCompile(regexExpr)
}

func (pw *PathWalker) reduceNonRegexSubPath() {
	for len(pw.remainingPathParts) > 0 &&
		pw.remainingPathParts[0] != "" &&
		!strings.Contains(pw.remainingPathParts[0], "{") {
		pw.matchedPath += "/" + pw.remainingPathParts[0]
		pw.remainingPathParts = pw.remainingPathParts[1:len(pw.remainingPathParts)]
	}
}

func (pw *PathWalker) MatchSubPath() string {
	return pw.matchedPath
}

func (pw *PathWalker) MatchDirectories(parts []string) []PathWalker {
	regex := pw.protocolPathPatternToRegex()
	var result []PathWalker
	for _, part := range parts {
		var matched bool
		matched = regex.MatchString(part)

		if matched {
			var newMatchedParameters = make(map[string]string)
			for k,v := range pw.matchedParameters {
				newMatchedParameters[k] = v
			}
			newResult := PathWalker{
				pattern:            pw.pattern,
				matchedPath:        pw.matchedPath + "/" + part,
				matchedParameters:  newMatchedParameters,
				remainingPathParts: pw.remainingPathParts[1:len(pw.remainingPathParts)],
			}
			match := regex.FindStringSubmatch(part)
			if i := regex.SubexpIndex("year"); i >= 0 {
				newResult.matchedParameters["{year}"] = match[i]
				newResult.matchedParameters["{yearShort"] = match[i][2:4]
			}
			if i := regex.SubexpIndex("month"); i >= 0 {
				newResult.matchedParameters["{month}"] = match[i]
				index, _ := strconv.ParseInt(match[i], 10, 32)
				newResult.matchedParameters["{monthName}"] = monthNames[index-1]
			}
			if i := regex.SubexpIndex("day"); i >= 0 {
				newResult.matchedParameters["{day}"] = match[i]
			}
			newResult.reduceNonRegexSubPath()
			result = append(result, newResult)
		}
	}
	return result
}

func (pw *PathWalker) MatchFiles(files []nextcloud_file.NextcloudFile) []PathWalker {
	regex := pw.protocolPathPatternToRegex()
	var result []PathWalker
	for _, file := range files {
		var matched bool
		matched = regex.MatchString(file.Name)

		if matched {
			var newMatchedParameters = make(map[string]string)
			for k,v := range pw.matchedParameters {
				newMatchedParameters[k] = v
			}
			fileC := file
			newResult := PathWalker{
				pattern:            pw.pattern,
				matchedPath:        pw.matchedPath + "/" + file.Name,
				file:               &fileC,
				matchedParameters:  newMatchedParameters,
				remainingPathParts: pw.remainingPathParts[1:len(pw.remainingPathParts)],
			}
			match := regex.FindStringSubmatch(file.Name)
			if i := regex.SubexpIndex("year"); i >= 0 {
				newResult.matchedParameters["{year}"] = match[i]
				newResult.matchedParameters["{yearShort"] = match[i][2:4]
			}
			if i := regex.SubexpIndex("month"); i >= 0 {
				newResult.matchedParameters["{month}"] = match[i]
				index, _ := strconv.ParseInt(match[i], 10, 32)
				newResult.matchedParameters["{monthName}"] = monthNames[index-1]
			}
			if i := regex.SubexpIndex("day"); i >= 0 {
				newResult.matchedParameters["{day}"] = match[i]
			}
			newResult.reduceNonRegexSubPath()
			result = append(result, newResult)
		}
	}
	return result
}

func (pw *PathWalker) Done() (*nextcloud_file.NextcloudFile, bool) {
	if pw.file != nil {
		return pw.file, true
	} else {
		return nil, false
	}
}

func (pw *PathWalker) AtFileLevel() bool {
	return len(pw.remainingPathParts) <= 1
}

func (pw *PathWalker) GetDate() (*time.Time, error) {
	year, err := strconv.ParseInt(pw.matchedParameters["{year}"], 10, 32)
	if err != nil {
		return nil, err
	}
	month, err := strconv.ParseInt(pw.matchedParameters["{month}"], 10, 32)
	if err != nil {
		return nil, err
	}
	day, err := strconv.ParseInt(pw.matchedParameters["{day}"], 10, 32)
	if err != nil {
		return nil, err
	}
	result := time.Date(int(year), time.Month(month), int(day), 0, 0, 0, 0, time.UTC)
	return &result, nil
}
