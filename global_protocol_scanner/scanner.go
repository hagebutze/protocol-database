package global_protocol_scanner

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"protocol-database/database"
	"protocol-database/nextcloud"
	"protocol-database/protocol_scanner"
	"sync"
)

type GlobalScanStatus string

const (
	GlobalScanDone       = "done"
	GlobalScanRunning    = "running"
	GlobalScanNotScanned = "not_scanned"
	GlobalScanFailed     = "failed"
)

type GlobalScannerStatus struct {
	Status              GlobalScanStatus
	Message             string
	NumProtocolsFound   int
	NumProtocolsScanned int
	NumErrors           int
}

type globalScannerStatusStore struct {
	mux         sync.Mutex
	status      GlobalScannerStatus
	changedCond *sync.Cond
}

func (s *globalScannerStatusStore) get() GlobalScannerStatus {
	s.mux.Lock()
	defer s.mux.Unlock()
	return s.status
}

func (s *globalScannerStatusStore) set(status GlobalScannerStatus) {
	s.mux.Lock()
	defer s.mux.Unlock()
	s.status = status
	s.changedCond.Broadcast()
}

type GlobalScanner struct {
	statusStore         globalScannerStatusStore
	dbClient            *database.DBClient
	ncClient            *nextcloud.Client
	protocolPathPattern string
}

func NewGlobalScanner(
	dbClient *database.DBClient,
	ncClient *nextcloud.Client,
	protocolPathPattern string) *GlobalScanner {

	return &GlobalScanner{
		globalScannerStatusStore{
			sync.Mutex{},
			GlobalScannerStatus{
				GlobalScanNotScanned,
				"scan ot run yet",
				0,
				0,
				0,
			},
			sync.NewCond(&sync.Mutex{}),
		},
		dbClient,
		ncClient,
		protocolPathPattern,
	}
}

func (s *GlobalScanner) Run() error {
	status := s.statusStore.get()
	if status.Status == GlobalScanRunning {
		return errors.New("scan already running")
	}
	ctx := context.Background()
	// Set statusStore to running
	s.statusStore.set(GlobalScannerStatus{GlobalScanRunning, "scan has been started", 0, 0, 0})
	go func() {
		protocolList, err := s.ncClient.ListAllProtocols(ctx, "pdf")
		if err != nil {
			s.statusStore.set(
				GlobalScannerStatus{
					GlobalScanFailed,
					fmt.Sprintf("error on listing protocols: %s", err.Error()),
					0,
					0,
					0,
				})
			return
		}
		s.statusStore.set(
			GlobalScannerStatus{
				GlobalScanRunning,
				fmt.Sprintf("%d protocols have been detected, resetting database", len(protocolList)),
				len(protocolList),
				0,
				0,
			})

		err = s.dbClient.EmptyTables()
		if err != nil {
			s.statusStore.set(GlobalScannerStatus{
				GlobalScanFailed,
				fmt.Sprintf("error on resetting tables: %s", err.Error()),
				0,
				0,
				0,
			})
			return
		}
		s.statusStore.set(
			GlobalScannerStatus{
				GlobalScanRunning,
				fmt.Sprintf("%d protocols have been detected, scanning", len(protocolList)),
				len(protocolList),
				0,
				0,
			})

		// Go through the protocols
		var errors []error
		for i, protocolPathAndDate := range protocolList {
			s.statusStore.set(
				GlobalScannerStatus{
					GlobalScanRunning,
					fmt.Sprintf("downloading protocolPath %d of %d: %s", i+1, len(protocolList), protocolPathAndDate.File.FullPath()),
					len(protocolList),
					i,
					len(errors),
				})

			tempDir, err := ioutil.TempDir("", "protocols")
			if err != nil {
				errors = append(errors, err)
				err = nil
				continue
			}
			localPath := path.Join(tempDir, "protocolPath.pdf")
			err = s.ncClient.DownloadProtocolByPath(ctx, localPath, protocolPathAndDate.File.FullPath())
			if err != nil {
				errors = append(errors, err)
				s.dbClient.StoreProtocolError(
					protocolPathAndDate.File, protocolPathAndDate.Date,
					fmt.Sprintf("error downloading protocol: %s", err.Error()),
				)
				os.RemoveAll(tempDir)
				err = nil
				continue
			}
			s.statusStore.set(
				GlobalScannerStatus{
					GlobalScanRunning,
					fmt.Sprintf("scanning protocolPath %d of %d: %s", i+1, len(protocolList), protocolPathAndDate.File.FullPath()),
					len(protocolList),
					i,
					len(errors),
				})

			protocol, err := protocol_scanner.ScanProtocolPdf(localPath, protocolPathAndDate.Date)
			if err != nil {
				errors = append(errors, err)
				s.dbClient.StoreProtocolError(
					protocolPathAndDate.File, protocolPathAndDate.Date,
					err.Error(),
				)
				os.RemoveAll(tempDir)
				err = nil
				continue
			}

			s.statusStore.set(
				GlobalScannerStatus{
					GlobalScanRunning,
					fmt.Sprintf("storing scan result for protocolPath %s", protocolPathAndDate.File.FullPath()),
					len(protocolList),
					i,
					len(errors),
				})
			err = s.dbClient.StoreFullProtocol(protocolPathAndDate.File, protocol)
			if err != nil {
				fmt.Printf("Error: unable to store protocol: %s\n", err.Error())
				errors = append(errors, err)
				os.RemoveAll(tempDir)
				err = nil
				continue
			}
			err = nil
			os.RemoveAll(tempDir)
		}
		s.statusStore.set(
			GlobalScannerStatus{
				GlobalScanDone,
				fmt.Sprintf("scanned %d of %d protocols (%d errors)", len(protocolList), len(protocolList), len(errors)),
				len(protocolList),
				len(protocolList),
				len(errors),
			})
	}()
	return nil
}

func (s *GlobalScanner) GetStatus() GlobalScannerStatus {
	return s.statusStore.get()
}

func (s *GlobalScanner) GetChangedCond() *sync.Cond {
	return s.statusStore.changedCond
}
