package api_functions

import (
	"encoding/json"
	"fmt"
	"net/http"
	"protocol-database/api/model"
	"protocol-database/database"
)

func (env* ApiEnv)  DecisionsGet(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	// Retrieve the decisions
	queryValues := r.URL.Query()
	startDate, withStartDate := queryValues["startDate"]
	endDate, withEndDate := queryValues["endDate"]

	// Result variables
	var err error
	var decisions []*database.DecisionWithProtocolInfo
	var totalNum int


	if !withStartDate && !withEndDate {
		decisions, totalNum, err = env.DBClient.GetAllDecisionsWithDates()
	}
	if !withStartDate && withEndDate {
		decisions, totalNum, err = env.DBClient.GetAllDecisionsWithDatesUntil(endDate[0])
	}
	if withStartDate && !withEndDate {
		decisions, totalNum, err = env.DBClient.GetAllDecisionsWithDatesAfter(startDate[0])
	}
	if withStartDate && withEndDate {
		decisions, totalNum, err = env.DBClient.GetAllDecisionsWithDatesBetween(startDate[0], endDate[0])
	}

	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}

	// Create the successful result
	var result swagger.PagedDecisions
	if len(decisions) == 0 {
		result = swagger.PagedDecisions{
			LastId: "0",
			NumItemsFound: 0,
			NumItemsReturned: 0,
			Decisions: []swagger.Decision{},
		}
	} else {
		var resultDecisions []swagger.Decision
		for _, c := range decisions {
			resultDecisions = append(resultDecisions,
				swagger.Decision{
					Id:    fmt.Sprintf("%d", c.Id),
					Date:  c.Date.Format("2006-01-02"),
					Text: c.Text,
					StartPage: int32(c.StartPage),
					EndPage: int32(c.EndPage),
					StartPosition: int32(c.StartPosition),
					EndPosition: int32(c.EndPosition),
				})
		}
		result = swagger.PagedDecisions{
			LastId: fmt.Sprintf("%d", decisions[len(decisions)-1].Id),
			NumItemsFound: int32(totalNum),
			NumItemsReturned: int32(len(decisions)),
			Decisions: resultDecisions,
		}
	}
	bytes, err := json.Marshal(result)
	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
	return
}
