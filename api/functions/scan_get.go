package api_functions

import (
	"encoding/json"
	"fmt"
	"net/http"
	swagger "protocol-database/api/model"
)

func (env* ApiEnv) ScanGet(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	// Just get the scan result
	status, message := env.ProtocolScanner.GetStatus()

	bytes, err := json.Marshal(swagger.ScanStatus{
		Status: string(status),
		Message: message,
	})
	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
	return

	w.WriteHeader(http.StatusOK)
	return
}
