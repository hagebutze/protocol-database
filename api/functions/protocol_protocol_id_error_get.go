package api_functions

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func (env *ApiEnv) ProtocolProtocolIdErrorGet(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	// Retrieve the protocol id
	vars := mux.Vars(r)
	protocolIdStr := vars["protocol_id"]
	protocolId, err := strconv.ParseInt(protocolIdStr, 10, 32)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid protocol id"))
		return
	}

	// Result variables
	result, err := env.DBClient.GetProtocolError(int(protocolId))
	bytes, err := json.Marshal(result)
	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
	return
}
