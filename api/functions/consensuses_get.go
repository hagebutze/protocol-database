package api_functions

import (
	"encoding/json"
	"fmt"
	"net/http"
	"protocol-database/api/model"
	"protocol-database/database"
)

func (env* ApiEnv)  ConsensusesGet(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	// Retrieve the consensuses
	queryValues := r.URL.Query()
	startDate, withStartDate := queryValues["startDate"]
	endDate, withEndDate := queryValues["endDate"]

	// Result variables
	var err error
	var consensuses []*database.ConsensusWithProtocolInfo
	var totalNum int


	if !withStartDate && !withEndDate {
		consensuses, totalNum, err = env.DBClient.GetAllConsensusesWithDates()
	}
	if !withStartDate && withEndDate {
		consensuses, totalNum, err = env.DBClient.GetAllConsensusesWithDatesUntil(endDate[0])
	}
	if withStartDate && !withEndDate {
		consensuses, totalNum, err = env.DBClient.GetAllConsensusesWithDatesAfter(startDate[0])
	}
	if withStartDate && withEndDate {
		consensuses, totalNum, err = env.DBClient.GetAllConsensusesWithDatesBetween(startDate[0], endDate[0])
	}

	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}

	// Create the successful result
	var result swagger.PagedConsensuses
	if len(consensuses) == 0 {
		result = swagger.PagedConsensuses{
			LastId: "0",
			NumItemsFound: 0,
			NumItemsReturned: 0,
			Consensuses: []swagger.Consensus{},
		}
	} else {
		var resultConsensuses []swagger.Consensus
		for _, c := range consensuses {
			resultConsensuses = append(resultConsensuses,
				swagger.Consensus{
					Id:    fmt.Sprintf("%d", c.Id),
					Date:  c.Date.Format("2006-01-02"),
					Text: c.Text,
					StartPage: int32(c.StartPage),
					EndPage: int32(c.EndPage),
					StartPosition: int32(c.StartPosition),
					EndPosition: int32(c.EndPosition),
				})
		}
		result = swagger.PagedConsensuses{
			LastId: fmt.Sprintf("%d", consensuses[len(consensuses)-1].Id),
			NumItemsFound: int32(totalNum),
			NumItemsReturned: int32(len(consensuses)),
			Consensuses: resultConsensuses,
		}
	}
	bytes, err := json.Marshal(result)
	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
	return
}
