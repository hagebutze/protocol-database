package api_functions

import (
	"protocol-database/database"
	"protocol-database/global_protocol_scanner"
)

// All data needed for api functions
type ApiEnv struct {
	DBClient *database.DBClient
	ProtocolScanner *global_protocol_scanner.GlobalScanner
	NextcloudUrl string
}