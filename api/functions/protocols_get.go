package api_functions

import (
	"encoding/json"
	"fmt"
	"net/http"
	"protocol-database/api/model"
	"protocol-database/database"
)

func (env* ApiEnv)  ProtocolsGet(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	// Retrieve the protocols
	queryValues := r.URL.Query()
	startDate, withStartDate := queryValues["startDate"]
	endDate, withEndDate := queryValues["endDate"]

	fmt.Println(startDate)
	fmt.Println(endDate)
	// Result variables
	var err error
	var protocols []*database.Protocol
	var totalNumProtocols int


	if !withStartDate && !withEndDate {
		protocols, totalNumProtocols, err = env.DBClient.GetAllProtocols()
	}
	if !withStartDate && withEndDate {
		protocols, totalNumProtocols, err = env.DBClient.GetAllProtocolsUntil(endDate[0])
	}
	if withStartDate && !withEndDate {
		protocols, totalNumProtocols, err = env.DBClient.GetAllProtocolsAfter(startDate[0])
	}
	if withStartDate && withEndDate {
		protocols, totalNumProtocols, err = env.DBClient.GetAllProtocolsBetween(startDate[0], endDate[0])
	}

	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}

	// Create the successful result
	var result swagger.ProtocolMetas
	if len(protocols) == 0 {
		result = swagger.ProtocolMetas{
			LastId: "0",
			NumItemsFound: 0,
			NumItemsReturned: 0,
			Protocols: []swagger.ProtocolMeta{},
		}
	} else {
		var resultProtocols []swagger.ProtocolMeta
		for _, p := range protocols {
			resultProtocols = append(resultProtocols,
				swagger.ProtocolMeta{
					Id:    fmt.Sprintf("%d", p.Id),
					Date:  p.Date.Format("2006-01-02"),
					Type_: p.Type,
					NextcloudLink: fmt.Sprintf("%s/remote.php/webdav%s", env.NextcloudUrl, p.NextcloudPath),
					HasErrors: p.Error != "",
				})
		}
		result = swagger.ProtocolMetas{
			LastId: fmt.Sprintf("%d", protocols[len(protocols)-1].Id),
			NumItemsFound: int32(totalNumProtocols),
			NumItemsReturned: int32(len(protocols)),
			Protocols: resultProtocols,
		}
	}
	bytes, err := json.Marshal(result)
	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
	return
}
