package api_functions

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"protocol-database/api/model"
	"strconv"
)

func (env* ApiEnv)  ProtocolProtocolIdChaptersGet(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	// Retrieve the protocol id
	vars := mux.Vars(r)
	chapterIdStr := vars["protocol_id"]
	chapterId, err := strconv.ParseInt(chapterIdStr, 10, 32)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid protocol id"))
		return
	}

	// Result variables
	chapters, err := env.DBClient.GetAllChaptersForProtocol(int(chapterId))

	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}

	// Create the successful result
	var result swagger.ProtocolChapterMetas
	if len(chapters) == 0 {
		result = swagger.ProtocolChapterMetas{
			Chapters: []swagger.ChapterMeta{},
		}
	} else {
		var resultChapters []swagger.ChapterMeta
		for _, c := range chapters {
			resultChapters = append(resultChapters,
				swagger.ChapterMeta{
					Id:    fmt.Sprintf("%d", c.Id),
					Title: c.Headline,
					StartPage: int32(c.StartPage),
					EndPage: int32(c.EndPage),
					StartPosition: int32(c.StartPosition),
					EndPosition: int32(c.EndPosition),
				})
		}
		result = swagger.ProtocolChapterMetas{
			Chapters: resultChapters,
		}
	}
	bytes, err := json.Marshal(result)
	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
	return
}