package api_functions

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"protocol-database/api/model"
	"strconv"
)

func (env* ApiEnv)  ProtocolProtocolIdDecisionsGet(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	// Retrieve the protocol id
	vars := mux.Vars(r)
	protocolIdStr := vars["protocol_id"]
	protocolId, err := strconv.ParseInt(protocolIdStr, 10, 32)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid protocol id"))
		return
	}

	// Result variables
	decisions, err := env.DBClient.GetAllDecisionsForProtocol(int(protocolId))

	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}

	// Create the successful result
	var result swagger.AllDecisions
	if len(decisions) == 0 {
		result = swagger.AllDecisions{
			Decisions: []swagger.Decision{},
		}
	} else {
		var resultDecisions []swagger.Decision
		for _, d := range decisions {
			resultDecisions = append(resultDecisions,
				swagger.Decision{
					Id:    fmt.Sprintf("%d", d.Id),
					Text: d.Text,
					StartPage: int32(d.StartPage),
					EndPage: int32(d.EndPage),
					StartPosition: int32(d.StartPosition),
					EndPosition: int32(d.EndPosition),
				})
		}
		result = swagger.AllDecisions{
			Decisions: resultDecisions,
		}
	}
	bytes, err := json.Marshal(result)
	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
	return
}