package api_functions

import (
	"encoding/json"
	"fmt"
	"net/http"
	swagger "protocol-database/api/model"
)

var GlobalScanStatus = swagger.ScanStatus {
	Status:  "not_scanned",
	Message: "Protocols have not been scanned yet",
}

func (env* ApiEnv)  ScanStartPost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	// Start the scan
	err := env.ProtocolScanner.Run()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}

	// Write that the current scan status
	bytes, err := json.Marshal(GlobalScanStatus)
	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
}
