package api_functions

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"protocol-database/api/model"
	"strconv"
)

func (env* ApiEnv)  ProtocolProtocolIdConsensusesGet(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	// Retrieve the protocol id
	vars := mux.Vars(r)
	protocolIdStr := vars["protocol_id"]
	protocolId, err := strconv.ParseInt(protocolIdStr, 10, 32)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid protocol id"))
		return
	}

	// Result variables
	consensuses, err := env.DBClient.GetAllConsensusesForProtocol(int(protocolId))

	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}

	// Create the successful result
	var result swagger.AllConsensuses
	if len(consensuses) == 0 {
		result = swagger.AllConsensuses{
			Consensuses: []swagger.Consensus{},
		}
	} else {
		var resultConsensuses []swagger.Consensus
		for _, c := range consensuses {
			resultConsensuses = append(resultConsensuses,
				swagger.Consensus{
					Id:    fmt.Sprintf("%d", c.Id),
					Text: c.Text,
					StartPage: int32(c.StartPage),
					EndPage: int32(c.EndPage),
					StartPosition: int32(c.StartPosition),
					EndPosition: int32(c.EndPosition),
				})
		}
		result = swagger.AllConsensuses{
			Consensuses: resultConsensuses,
		}
	}
	bytes, err := json.Marshal(result)
	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
	return
}