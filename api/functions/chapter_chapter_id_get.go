package api_functions

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"protocol-database/api/model"
	"strconv"
)

func (env* ApiEnv) ChapterChapterIdGet(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	vars := mux.Vars(r)
	// Retrieve the chapter
	chapterIdStr := vars["chapter_id"]
	chapterId, err := strconv.ParseInt(chapterIdStr, 10, 32)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid chapter id"))
		return
	}

	// Get the chapter
	chapter, err := env.DBClient.GetChapter(int(chapterId))

	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}

	// Create the successful result
	result := swagger.Chapter{
		Id:            fmt.Sprintf("%d", chapter.Id),
		Title:         chapter.Headline,
		Text:          chapter.Text,
		StartPage:     int32(chapter.StartPage),
		EndPage:       int32(chapter.EndPage),
		StartPosition: int32(chapter.StartPosition),
		EndPosition:   int32(chapter.EndPosition),
	}
	bytes, err := json.Marshal(result)
	// Check error
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("error: %v", err)))
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
	return
}