package nextcloud

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"github.com/beevik/etree"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"protocol-database/global_protocol_scanner/path_pattern"
	"protocol-database/nextcloud_file"
	"strconv"
	"strings"
	"time"
)

var monthNames = []string{
	"Januar",
	"Februar",
	"März",
	"April",
	"Mai",
	"Juni",
	"Juli",
	"August",
	"September",
	"Oktober",
	"November",
	"Dezember",
}

func monthNumberToName(number string) (string, error) {
	intMonth, err := strconv.ParseInt(number, 10, 32)
	if err != nil {
		return "", err
	}
	if intMonth <= 0 || intMonth > 12 {
		return "", errors.New(fmt.Sprintf("invalid month: %s", number))
	}
	return monthNames[intMonth-1], nil
}

func (c *Client) CreateFolder(ctx context.Context, path string) error {
	// Remove starting /
	if path[0] == '/' {
		path = path[1:]
	}
	pathParts := strings.Split(path, "/")
	// Create the full path, directory for directory
	for pathDepth := 1; pathDepth <= len(pathParts); pathDepth++ {
		currentSubPath := strings.Join(pathParts[:pathDepth], "/")
		urlPath := fmt.Sprintf("%s/remote.php/webdav/%s", c.Url, currentSubPath)

		// Run the MKCOL request
		req, err := http.NewRequestWithContext(ctx, "MKCOL", urlPath, nil)
		if err != nil {
			return err
		}
		req.SetBasicAuth(c.username, c.password)

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return err
		}
		b, _ := ioutil.ReadAll(resp.Body)
		err = checkWebDavBodyForError(b, true)

		if err != nil {
			return err
		}
	}
	return nil
}



func (c *Client) ListDirectory(ctx context.Context, dirPath string) (files []nextcloud_file.NextcloudFile, directories []string, err error) {
	payload := `<?xml version="1.0"?>
<d:propfind  xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns" xmlns:nc="http://nextcloud.org/ns" xmlns:ocs="http://open-collaboration-services.org/ns">
  <d:prop>
    <d:getcontenttype />
    <d:resourcetype />
    <oc:fileid />
  </d:prop>
</d:propfind>
`
	// List directory
	urlPath := fmt.Sprintf("%s/remote.php/dav/files/%s/%s", c.Url, c.username, dirPath)

	// Get the properties
	req, err := http.NewRequestWithContext(ctx, "PROPFIND", urlPath, bytes.NewBufferString(payload))
	if err != nil {
		return
	}
	req.SetBasicAuth(c.username, c.password)
	resp, err := http.DefaultClient.Do(req)

	xmlDoc := etree.NewDocument()
	_, err = xmlDoc.ReadFrom(resp.Body)
	if err != nil {
		return
	}

	err = checkWebDavXMLForError(xmlDoc, false)
	if err != nil {
		return
	}

	root := xmlDoc.SelectElement("d:multistatus")
	if root == nil {
		err = errors.New("multistatus not found in response")
		return
	}

	directories = []string{}
	files = []nextcloud_file.NextcloudFile{}

	for _, response := range root.SelectElements("d:response") {
		href := response.SelectElement("d:href")
		if href == nil {
			continue
		}
		for _, propstat := range response.SelectElements("d:propstat") {
			prop := propstat.SelectElement("d:prop")
			if prop == nil {
				continue
			}
			resourcetype := prop.SelectElement("d:resourcetype")
			if resourcetype == nil {
				continue
			}
			isDirectory := resourcetype.SelectElement("d:collection") != nil

			var name string
			if isDirectory {
				name, err = url.QueryUnescape(path.Base(strings.TrimRight(href.Text(), "/")))
				if err != nil {
					return
				}
				directories = append(directories, name)
			} else {
				fileidElement := prop.SelectElement("oc:fileid")
				fileid := ""
				if fileidElement != nil {
					fileid = fileidElement.Text()
				}
				name, err = url.QueryUnescape(path.Base(href.Text()))
				if err != nil {
					return
				}
				files = append(files, nextcloud_file.NextcloudFile{
					Name: name,
					Link: fmt.Sprintf("%s/apps/files/?dir=%s&fileid=%s#pdfviewer",c.Url,dirPath,fileid),
					Directory: dirPath,
				})
			}
		}
	}
	return
}

type ProtocolPathAndDate struct {
	File nextcloud_file.NextcloudFile
	Date time.Time
}

func MakeUnboundedQueue() (chan<- path_pattern.PathWalker, <-chan path_pattern.PathWalker) {
	in := make(chan path_pattern.PathWalker)
	out := make(chan path_pattern.PathWalker)
	go func() {
		var inQueue []path_pattern.PathWalker

		onIncoming := func(val path_pattern.PathWalker, ok bool) {
			if !ok {
				in = nil
			} else {
				inQueue = append(inQueue, val)
			}
		}

		for len(inQueue) > 0 || in != nil {
			if len(inQueue) > 0 {
				select {
				case oc, ok := <-in:
					onIncoming(oc, ok)
				case out <- inQueue[0]:
					if out != nil {
						inQueue = inQueue[1:]
					}
				}
			} else {
				select {
				case oc, ok := <-in:
					onIncoming(oc, ok)
				}
			}
		}
		close(out)
	}()

	return in, out
}

func (c *Client) listAllProtocolsWorker(ctx context.Context, inputC <-chan path_pattern.PathWalker, outputC chan<- []path_pattern.PathWalker, errorC chan<- error) {
	for pathWalker := range inputC {
		files, subDirs, err := c.ListDirectory(ctx, pathWalker.MatchSubPath())
		if err != nil {
			errorC <- err
			return
		}

		if !pathWalker.AtFileLevel() {
			subDirWalkers := pathWalker.MatchDirectories(subDirs)
			outputC <- subDirWalkers
		} else {
			// Return the files
			fileWalkers := pathWalker.MatchFiles(files)
			outputC <- fileWalkers
		}
	}
}

func (c *Client) ListAllProtocols(ctx context.Context, ext string) (result []ProtocolPathAndDate, err error) {
	bufferedInputChannel, inputChannel := MakeUnboundedQueue()
	outputChannel := make(chan []path_pattern.PathWalker)
	errorChannel := make(chan error)

	for i := 0; i < 10; i++ {
		go c.listAllProtocolsWorker(ctx, inputChannel, outputChannel, errorChannel)
	}

	// Input first walker
	pathWalker := path_pattern.NewPathWalker(c.protocolsPathPattern, &ext)
	bufferedInputChannel <- pathWalker
	numWaitingWorkerRequests := 1

	var allErrors []error

	for numWaitingWorkerRequests > 0 {
		select {
		case requestOutputWalkers := <-outputChannel:
			numWaitingWorkerRequests -= 1
			for _, walker := range requestOutputWalkers {
				file, done := walker.Done()
				if done {
					// We are done here!
					d, err := walker.GetDate()
					if err != nil {
						allErrors = append(allErrors, err)
					} else {
						result = append(result, ProtocolPathAndDate{
							File: *file,
							Date: *d,
						})
					}
				} else {
					// Add another input to the channel
					numWaitingWorkerRequests += 1
					bufferedInputChannel <- walker
				}
			}
			break
		case err := <-errorChannel:
			numWaitingWorkerRequests -= 1
			allErrors = append(allErrors, err)
			break
		}
	}

	close(bufferedInputChannel)
	close(outputChannel)
	close(errorChannel)
	if len(allErrors) > 0 {
		err = allErrors[0]
	}
	return
}

func (c *Client) protocolPath(day string, monthNumber string, year string, fileExtensions string) (string, error) {
	monthName, err := monthNumberToName(monthNumber)
	if err != nil {
		return "", err
	}
	path := strings.ReplaceAll(
		strings.ReplaceAll(
			strings.ReplaceAll(
				strings.ReplaceAll(
					strings.ReplaceAll(c.protocolsPathPattern, "{year}", year),
					"{month}", monthNumber),
				"{monthName}", monthName),
			"{day}", day),
		"{ext}", fileExtensions)
	return path, nil
}

func (c *Client) CreateProtocolFolder(ctx context.Context, day string, monthNumber string, year string, fileExtensions string) error {
	pPath, err := c.protocolPath(day, monthNumber, year, fileExtensions)
	if err != nil {
		return err
	}
	err = c.CreateFolder(ctx, path.Dir(pPath))
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) UploadProtocol(ctx context.Context, localPath string, day string, monthNumber string, year string, fileExtensions string) error {
	pPath, err := c.protocolPath(day, monthNumber, year, fileExtensions)
	if err != nil {
		return err
	}
	// Create the folder
	err = c.CreateFolder(ctx, path.Dir(pPath))
	if err != nil {
		return err
	}
	// Upload the protocol
	usernamePassword := fmt.Sprintf("%s:%s", c.username, c.password)
	urlPath := fmt.Sprintf("%s/remote.php/dav/files/%s%s", c.Url, c.username, pPath)
	cmd := exec.Command("curl", "-f", "-u", usernamePassword, "-T", localPath, urlPath)
	err = cmd.Run()
	if err != nil {
		return err
	}
	return nil
}

func checkWebDavXMLForError(xmlDoc *etree.Document, ignoreAlreadyExistError bool) error {
	root := xmlDoc.SelectElement("d:error")
	if root == nil {
		return nil
	}
	if ignoreAlreadyExistError &&
		root.SelectElement("s:message").Text() == "The resource you tried to create already exists" {
		return nil
	}
	return errors.New(root.SelectElement("s:message").Text())
}

func checkWebDavBodyForError(body []byte, ignoreAlreadyExistError bool) error {
	xmlDoc := etree.NewDocument()
	err := xmlDoc.ReadFromBytes(body)
	if err != nil {
		// Its not an error, its not even xml
		return nil
	}
	return checkWebDavXMLForError(xmlDoc, ignoreAlreadyExistError)
}

func (c *Client) DownloadProtocol(ctx context.Context, localPath string, day string, monthNumber string, year string, ext string) error {
	pPath, err := c.protocolPath(day, monthNumber, year, ext)
	if err != nil {
		return err
	}
	// Download the protocol
	return c.DownloadProtocolByPath(ctx, localPath, pPath)
}

func (c *Client) DownloadProtocolByPath(ctx context.Context, localPath string, protocolPath string) error {
	// Download the protocol
	urlPath := fmt.Sprintf("%s/remote.php/webdav%s", c.Url, protocolPath)

	// Get the file
	req, err := http.NewRequestWithContext(ctx, "GET", urlPath, nil)
	if err != nil {
		return err
	}
	req.SetBasicAuth(c.username, c.password)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	// Check the body whether its an xml error document
	err = checkWebDavBodyForError(body, false)
	if err != nil {
		return err
	}

	outfile, err := os.Create(localPath)
	if err != nil {
		return err
	}
	defer outfile.Close()
	_, err = io.Copy(outfile, bytes.NewBuffer(body))
	if err != nil {
		return err
	}
	return nil
}
