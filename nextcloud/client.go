package nextcloud

import "protocol-database/config"

type Client struct {
	Url string
	protocolsPathPattern string
	username string
	password string
}

func NewClient(c *config.Config) *Client {
	return &Client {
		Url: c.NextCloud.Url,
		protocolsPathPattern: c.NextCloud.ProtocolsPathPattern,
		username: c.NextCloud.Username,
		password: c.NextCloud.Password,
	}
}
