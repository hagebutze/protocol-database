#!/bin/env bash

docker run --rm --name some-mysql -e MYSQL_RANDOM_ROOT_PASSWORD="true" -e MYSQL_DATABASE="protocols" -e MYSQL_USER="protocol_database" -e MYSQL_PASSWORD="12345678" -p 3306:3306 -d mysql