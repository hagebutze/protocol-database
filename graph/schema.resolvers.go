package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"protocol-database/database"
	"protocol-database/graph/generated"
	"protocol-database/graph/model"
	"strconv"
)

func (r *chapterResolver) Text(ctx context.Context, obj *model.Chapter) (string, error) {
	chapterId, err := strconv.ParseInt(obj.ID, 10, 32)
	if err != nil {
		return "", err
	}

	// Get the chapters
	chapter, err := r.dbClient.GetChapter(int(chapterId))
	return chapter.Text, err
}

func (r *mutationResolver) StartScan(ctx context.Context) (*model.ScanStatus, error) {
	// Start the scan
	err := r.protocolScanner.Run()
	if err != nil {
		return nil, err
	}

	// Write that the current scan status
	status := r.protocolScanner.GetStatus()
	return &model.ScanStatus{
		Status:              string(status.Status),
		Message:             status.Message,
		NumProtocolsFound:   status.NumProtocolsFound,
		NumProtocolsScanned: status.NumProtocolsScanned,
		NumProtocolsFailed:  status.NumErrors,
	}, nil
}

func (r *mutationResolver) RescanProtocol(ctx context.Context, protocolID string) (*model.ScanStatus, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *protocolResolver) Chapters(ctx context.Context, obj *model.Protocol) ([]*model.Chapter, error) {
	protocolId, err := strconv.ParseInt(obj.ID, 10, 32)
	if err != nil {
		return nil, err
	}

	// Get the chapters
	chapters, err := r.dbClient.GetAllChaptersForProtocol(int(protocolId))

	// Check error
	if err != nil {
		return nil, err
	}

	// Create the successful result
	result := make([]*model.Chapter, len(chapters))
	for i, chapter := range chapters {
		result[i] = &model.Chapter{
			ID:            fmt.Sprintf("%d", chapter.Id),
			Headline:      chapter.Headline,
			Text:          "",
			StartPage:     chapter.StartPage,
			EndPage:       chapter.EndPage,
			StartPosition: chapter.StartPosition,
			EndPosition:   chapter.EndPosition,
		}
	}
	return result, nil
}

func (r *protocolResolver) Consensuses(ctx context.Context, obj *model.Protocol) ([]*model.Consensus, error) {
	protocolId, err := strconv.ParseInt(obj.ID, 10, 32)
	if err != nil {
		return nil, err
	}

	// Get the Consensuses
	consensuses, err := r.dbClient.GetAllConsensusesForProtocol(int(protocolId))

	// Check error
	if err != nil {
		return nil, err
	}

	// Create the successful result
	result := make([]*model.Consensus, len(consensuses))
	for i, consensus := range consensuses {
		result[i] = &model.Consensus{
			ID:            fmt.Sprintf("%d", consensus.Id),
			Text:          consensus.Text,
			StartPage:     consensus.StartPage,
			EndPage:       consensus.EndPage,
			StartPosition: consensus.StartPosition,
			EndPosition:   consensus.EndPosition,
		}
	}
	return result, nil
}

func (r *protocolResolver) Decisions(ctx context.Context, obj *model.Protocol) ([]*model.Decision, error) {
	protocolId, err := strconv.ParseInt(obj.ID, 10, 32)
	if err != nil {
		return nil, err
	}

	// Get the Decisions
	decisions, err := r.dbClient.GetAllDecisionsForProtocol(int(protocolId))

	// Check error
	if err != nil {
		return nil, err
	}

	// Create the successful result
	result := make([]*model.Decision, len(decisions))
	for i, consensus := range decisions {
		result[i] = &model.Decision{
			ID:            fmt.Sprintf("%d", consensus.Id),
			Text:          consensus.Text,
			StartPage:     consensus.StartPage,
			EndPage:       consensus.EndPage,
			StartPosition: consensus.StartPosition,
			EndPosition:   consensus.EndPosition,
		}
	}
	return result, nil
}

func (r *queryResolver) ProtocolQuery(ctx context.Context, limit *int, cursor *string, minDate *string, maxDate *string) (*model.ProtocolQueryResult, error) {
	// Result variables
	var err error
	var protocols []*database.Protocol
	var totalNumProtocols int

	if minDate == nil && maxDate == nil {
		protocols, totalNumProtocols, err = r.dbClient.GetAllProtocols()
	}
	if minDate == nil && maxDate != nil {
		protocols, totalNumProtocols, err = r.dbClient.GetAllProtocolsUntil(*maxDate)
	}
	if minDate != nil && maxDate == nil {
		protocols, totalNumProtocols, err = r.dbClient.GetAllProtocolsAfter(*minDate)
	}
	if minDate != nil && maxDate != nil {
		protocols, totalNumProtocols, err = r.dbClient.GetAllProtocolsBetween(*minDate, *maxDate)
	}

	// Check error
	if err != nil {
		return nil, err
	}

	// Create the successful result
	newCursor := ""
	if len(protocols) > 0 {
		newCursor = fmt.Sprintf("%d", protocols[len(protocols)-1].Id)
	}
	result := &model.ProtocolQueryResult{
		totalNumProtocols,
		make([]*model.Protocol, len(protocols)),
		newCursor,
	}
	for i, p := range protocols {
		result.Protocols[i] = &model.Protocol{
			ID:            fmt.Sprintf("%d", p.Id),
			Date:          p.Date.Format("2006-01-02"),
			Type:          p.Type,
			NextcloudLink: p.NextcloudLink,
			HasErrors:     p.Error != "",
			Error:         p.Error,
			Chapters:      nil,
			Consensuses:   nil,
			Decisions:     nil,
		}
	}
	return result, nil
}

func (r *queryResolver) Protocol(ctx context.Context, id string) (*model.Protocol, error) {
	protocolId, err := strconv.ParseInt(id, 10, 32)
	if err != nil {
		return nil, err
	}

	// Get the protocol
	protocol, err := r.dbClient.GetProtocol(int(protocolId))

	// Check error
	if err != nil {
		return nil, err
	}

	// Create the successful result
	result := &model.Protocol{
		ID:            fmt.Sprintf("%d", protocol.Id),
		Date:          protocol.Date.Format("2006-01-02"),
		Type:          protocol.Type,
		NextcloudLink: protocol.NextcloudLink,
		NextcloudPath: protocol.NextcloudPath,
		HasErrors:     protocol.Error != "",
		Error:         protocol.Error,
		Chapters:      nil,
		Consensuses:   nil,
		Decisions:     nil,
	}
	return result, nil
}

func (r *queryResolver) Chapter(ctx context.Context, id string) (*model.Chapter, error) {
	chapterId, err := strconv.ParseInt(id, 10, 32)
	if err != nil {
		return nil, err
	}

	// Get the chapter
	chapter, err := r.dbClient.GetChapter(int(chapterId))

	// Check error
	if err != nil {
		return nil, err
	}

	// Create the successful result
	result := &model.Chapter{
		ID:            fmt.Sprintf("%d", chapter.Id),
		Headline:      chapter.Headline,
		Text:          chapter.Text,
		StartPage:     chapter.StartPage,
		EndPage:       chapter.EndPage,
		StartPosition: chapter.StartPosition,
		EndPosition:   chapter.EndPosition,
	}
	return result, nil
}

func (r *queryResolver) Consensuses(ctx context.Context, limit *int, cursor *string, minDate *string, maxDate *string) (*model.ConsensusQueryResult, error) {
	// Result variables
	var err error
	var consensuses []*database.ConsensusWithProtocolInfo
	var totalNum int

	if minDate == nil && maxDate == nil {
		consensuses, totalNum, err = r.dbClient.GetAllConsensusesWithProtocolInfo()
	}
	if minDate == nil && maxDate != nil {
		consensuses, totalNum, err = r.dbClient.GetAllConsensusesWithProtocolInfoUntil(*maxDate)
	}
	if minDate != nil && maxDate == nil {
		consensuses, totalNum, err = r.dbClient.GetAllConsensusesWithProtocolInfoAfter(*minDate)
	}
	if minDate != nil && maxDate != nil {
		consensuses, totalNum, err = r.dbClient.GetAllConsensusesWithProtocolInfoBetween(*minDate, *maxDate)
	}

	// Check error
	if err != nil {
		return nil, err
	}

	// Create the successful result
	newCursor := ""
	if len(consensuses) > 0 {
		newCursor = fmt.Sprintf("%d", consensuses[len(consensuses)-1].Id)
	}
	result := &model.ConsensusQueryResult{
		totalNum,
		make([]*model.Consensus, len(consensuses)),
		newCursor,
	}
	for i, c := range consensuses {
		result.Consensuses[i] = &model.Consensus{
			ID:            fmt.Sprintf("%d", c.Id),
			ProtocolID:    fmt.Sprintf("%d", c.ProtocolId),
			ChapterID:     fmt.Sprintf("%d", c.ChapterId),
			Date:          c.Date.Format("2006-01-02"),
			NextcloudLink: c.NextcloudLink,
			Text:          c.Text,
			StartPage:     c.StartPage,
			EndPage:       c.EndPage,
			StartPosition: c.StartPosition,
			EndPosition:   c.EndPosition,
		}
	}
	return result, nil
}

func (r *queryResolver) Consensus(ctx context.Context, id string) (*model.Consensus, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Decisions(ctx context.Context, limit *int, cursor *string, minDate *string, maxDate *string) (*model.DecisionQueryResult, error) {
	// Result variables
	var err error
	var decisions []*database.DecisionWithProtocolInfo
	var totalNum int

	if minDate == nil && maxDate == nil {
		decisions, totalNum, err = r.dbClient.GetAllDecisionsWithProtocolInfo()
	}
	if minDate == nil && maxDate != nil {
		decisions, totalNum, err = r.dbClient.GetAllDecisionsWithProtocolInfoUntil(*maxDate)
	}
	if minDate != nil && maxDate == nil {
		decisions, totalNum, err = r.dbClient.GetAllDecisionsWithProtocolInfoAfter(*minDate)
	}
	if minDate != nil && maxDate != nil {
		decisions, totalNum, err = r.dbClient.GetAllDecisionsWithProtocolInfoBetween(*minDate, *maxDate)
	}

	// Check error
	if err != nil {
		return nil, err
	}

	// Create the successful result
	newCursor := ""
	if len(decisions) > 0 {
		newCursor = fmt.Sprintf("%d", decisions[len(decisions)-1].Id)
	}
	result := &model.DecisionQueryResult{
		totalNum,
		make([]*model.Decision, len(decisions)),
		newCursor,
	}
	for i, c := range decisions {
		result.Decisions[i] = &model.Decision{
			ID:            fmt.Sprintf("%d", c.Id),
			ProtocolID:    fmt.Sprintf("%d", c.ProtocolId),
			ChapterID:     fmt.Sprintf("%d", c.ChapterId),
			Date:          c.Date.Format("2006-01-02"),
			NextcloudLink: c.NextcloudLink,
			Text:          c.Text,
			StartPage:     c.StartPage,
			EndPage:       c.EndPage,
			StartPosition: c.StartPosition,
			EndPosition:   c.EndPosition,
		}
	}
	return result, nil
}

func (r *queryResolver) Decision(ctx context.Context, id string) (*model.Decision, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) MonthsWithProtocols(ctx context.Context) ([]*model.MonthWithProtocols, error) {
	// Get the information
	months, err := r.dbClient.GetMonthsWithProtocols()

	// Check error
	if err != nil {
		return nil, err
	}

	// Create the successful result
	result := make([]*model.MonthWithProtocols, len(months))
	for i, month := range months {
		result[i] = &model.MonthWithProtocols{
			Year:         month.Year(),
			Month:        month.Month(),
			NumProtocols: month.NumProtocols,
		}
	}
	return result, nil
}

func (r *queryResolver) ScanStatus(ctx context.Context) (*model.ScanStatus, error) {
	// Write that the current scan status
	status := r.protocolScanner.GetStatus()
	return &model.ScanStatus{
		Status:              string(status.Status),
		Message:             status.Message,
		NumProtocolsFound:   status.NumProtocolsFound,
		NumProtocolsScanned: status.NumProtocolsScanned,
		NumProtocolsFailed:  status.NumErrors,
	}, nil
}

func (r *subscriptionResolver) ScanStatusChanged(ctx context.Context) (<-chan *model.ScanStatus, error) {
	c := make(chan *model.ScanStatus)
	index := r.statusChangedChannelList.Add(&c)
	go func() {
		// Wait for done
		<-ctx.Done()
		r.statusChangedChannelList.Remove(index)
		close(c)
	}()
	return c, nil
}

// Chapter returns generated.ChapterResolver implementation.
func (r *Resolver) Chapter() generated.ChapterResolver { return &chapterResolver{r} }

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Protocol returns generated.ProtocolResolver implementation.
func (r *Resolver) Protocol() generated.ProtocolResolver { return &protocolResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Subscription returns generated.SubscriptionResolver implementation.
func (r *Resolver) Subscription() generated.SubscriptionResolver { return &subscriptionResolver{r} }

type chapterResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
type protocolResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type subscriptionResolver struct{ *Resolver }
