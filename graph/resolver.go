package graph

import (
	"math/rand"
	"protocol-database/config"
	"protocol-database/database"
	"protocol-database/global_protocol_scanner"
	"protocol-database/graph/model"
	"protocol-database/nextcloud"
	"sync"
)

type StatusChangedChannelList struct {
	mux *sync.Mutex
	channels map[int]*chan *model.ScanStatus
}

func (cl* StatusChangedChannelList) Send(m *model.ScanStatus) {
	cl.mux.Lock()
	defer cl.mux.Unlock()
	for _, channel := range cl.channels {
		*channel <- m
	}
}

func (cl* StatusChangedChannelList) Add(c *chan *model.ScanStatus) int {
	cl.mux.Lock()
	defer cl.mux.Unlock()
	// Find free int
	index := rand.Int()
	for ;; {
		_, ok := cl.channels[index]
		if ok {
			index = rand.Int()
		} else {
			break
		}
	}
	cl.channels[index] = c
	return index
}

func (cl* StatusChangedChannelList) Remove(index int) {
	cl.mux.Lock()
	defer cl.mux.Unlock()
	delete(cl.channels, index)
}

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.
func NewResolver(
	config *config.Config,
	dbClient *database.DBClient,
	ncClient *nextcloud.Client,
	pScanner *global_protocol_scanner.GlobalScanner) *Resolver {

	statusChangedChannelList := StatusChangedChannelList{
		&sync.Mutex{},
		make(map[int]*chan *model.ScanStatus),
	}

	// Start pumping scanner status changed events
	go func() {
		for ;; {
			cond := pScanner.GetChangedCond()
			cond.L.Lock()
			cond.Wait()
			status := pScanner.GetStatus()
			statusChangedChannelList.Send(&model.ScanStatus{
				Status:              string(status.Status),
				Message:             status.Message,
				NumProtocolsFound:   status.NumProtocolsFound,
				NumProtocolsScanned: status.NumProtocolsScanned,
				NumProtocolsFailed:  status.NumErrors,
			})
			cond.L.Unlock()
		}
	}()

	return &Resolver {
		config:          config,
		dbClient:        dbClient,
		ncClient:        ncClient,
		protocolScanner: pScanner,
		statusChangedChannelList: statusChangedChannelList,
	}
}

type Resolver struct{
	config *config.Config
	dbClient *database.DBClient
	ncClient *nextcloud.Client
	protocolScanner *global_protocol_scanner.GlobalScanner
	statusChangedChannelList StatusChangedChannelList
}
