package nextcloud_file

type NextcloudFile struct {
	Name      string
	Directory string
	Link      string
}

func (f* NextcloudFile) FullPath() string {
	return f.Directory + "/" + f.Name
}
