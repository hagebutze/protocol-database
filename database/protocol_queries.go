package database

import (
	"database/sql"
)

func (db* DBClient) GetProtocol(id int) (*Protocol, error){
	var protocol Protocol
	err := db.driver.QueryRow("SELECT id, nextcloud_link, nextcloud_path, error, type, date from protocol WHERE id = ?", id).
		Scan(&protocol.Id, &protocol.NextcloudLink, &protocol.NextcloudPath, &protocol.Error, &protocol.Type, &protocol.Date)
	if err != nil {
		return nil, err
	}
	return &protocol, nil
}

func (db* DBClient) GetProtocolForConsensus(id int) (*Protocol, error) {
	var protocol Protocol
	err := db.driver.QueryRow("SELECT p.id, p.nextcloud_link, p.nextcloud_path, p.error, p.type, p.date from protocol AS p INNER JOIN consensus AS c ON p.id = c.protocol_id WHERE  c.id = ?", id).
		Scan(&protocol.Id, &protocol.NextcloudLink, &protocol.NextcloudPath, &protocol.Error, &protocol.Type, &protocol.Date)
	if err != nil {
		return nil, err
	}
	return &protocol, nil
}

func (db* DBClient) GetProtocolForDecision(id int) (*Protocol, error) {
	var protocol Protocol
	err := db.driver.QueryRow("SELECT p.id, p.nextcloud_link, p.nextcloud_path, p.error, p.type, p.date from protocol AS p INNER JOIN decision AS d ON p.id = d.protocol_id WHERE  d.id = ?", id).
		Scan(&protocol.Id, &protocol.NextcloudLink, &protocol.NextcloudPath, &protocol.Error, &protocol.Type, &protocol.Date)
	if err != nil {
		return nil, err
	}
	return &protocol, nil
}

func (db* DBClient) GetProtocolError(id int) (string, error){
	var result string
	err := db.driver.QueryRow("SELECT error from protocol WHERE id = ?", id).
		Scan(&result)
	if err != nil {
		return "", err
	}
	return result, nil
}

func (db* DBClient) getProtocolsFromRows(rows *sql.Rows) ([]*Protocol, int, error) {
	var protocols []*Protocol

	for rows.Next() {
		var protocol Protocol
		err := rows.Scan(&protocol.Id, &protocol.Type, &protocol.Date, &protocol.NextcloudLink, &protocol.NextcloudPath, &protocol.Error)
		if err != nil {
			return nil, 0, err
		}
		protocols = append(protocols, &protocol)
	}
	return protocols, len(protocols), nil
}

func (db* DBClient) GetAllProtocols() ([]*Protocol, int, error) {
	rows, err:= db.driver.Query("SELECT id, type, date, nextcloud_link, nextcloud_path, error from protocol")

	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	protocols, num, err := db.getProtocolsFromRows(rows)
	return protocols, num, err
}

func (db* DBClient) GetAllProtocolsUntil(lastDate string) ([]*Protocol, int, error) {
	rows, err:= db.driver.Query("SELECT id, type, date, nextcloud_link, nextcloud_path, error from protocol WHERE date < ?", lastDate)

	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()
	protocols, num, err := db.getProtocolsFromRows(rows)
	return protocols, num, err
}

func (db* DBClient) GetAllProtocolsAfter(firstDate string) ([]*Protocol, int, error) {
	rows, err:= db.driver.Query("SELECT id, type, date, nextcloud_link, nextcloud_path, error from protocol WHERE date >= ?", firstDate)
	defer rows.Close()

	if err != nil {
		return nil, 0, err
	}
	protocols, num, err := db.getProtocolsFromRows(rows)
	return protocols, num, err
}

func (db* DBClient) GetAllProtocolsBetween(firstDate string, lastDate string) ([]*Protocol, int, error) {
	rows, err:= db.driver.Query("SELECT id, type, date, nextcloud_link, nextcloud_path, error from protocol WHERE date BETWEEN ? AND ?", firstDate, lastDate)
	defer rows.Close()

	if err != nil {
		return nil, 0, err
	}
	protocols, num, err := db.getProtocolsFromRows(rows)
	return protocols, num, err
}

