package database

import "database/sql"

func (db* DBClient) getChaptersWithoutTextFromRows(rows *sql.Rows) ([]*ChapterWithoutText, error) {
	var chapters []*ChapterWithoutText

	for rows.Next() {
		var chapter ChapterWithoutText
		err := rows.Scan(
			&chapter.Id,
			&chapter.ProtocolId,
			&chapter.Headline,
			&chapter.StartPage,
			&chapter.EndPage,
			&chapter.StartPosition,
			&chapter.EndPosition,
		)
		if err != nil {
			return nil, err
		}
		chapters = append(chapters, &chapter)
	}
	return chapters, nil
}

func (db* DBClient) GetAllChaptersForProtocol(protocolId int) ([]*ChapterWithoutText, error) {
	rows, err:= db.driver.Query(
		`SELECT id, protocol_id, headline, start_page, end_page, start_position, end_position
               FROM chapter
               WHERE protocol_id = ?`, protocolId)
	defer rows.Close()

	if err != nil {
		return nil, err
	}
	chapters, err := db.getChaptersWithoutTextFromRows(rows)
	return chapters, err
}

func (db* DBClient) GetChapter(id int) (*Chapter, error) {
	var chapter Chapter
	err := db.driver.QueryRow(
		`SELECT id, protocol_id, headline, text, start_page, start_position, end_page, end_position
               FROM chapter
               WHERE id = ?`, id).
		Scan(&chapter.Id, &chapter.ProtocolId, &chapter.Headline, &chapter.Text, &chapter.StartPage, &chapter.StartPosition, &chapter.EndPage, &chapter.EndPosition)
	if err != nil {
		return nil, err
	}
	return &chapter, nil
}