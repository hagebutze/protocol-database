package database

import (
	"context"
	"fmt"
	"protocol-database/nextcloud_file"
	"protocol-database/protocol_scanner"
	"time"
)

func (db* DBClient) StoreProtocol(protocol *Protocol) (int64, error) {
	query := "INSERT INTO protocol(date, nextcloud_link, nextcloud_path, error, type) VALUES (?, ?, ?, ?, ?)"
	ctx, cancelfunc := context.WithTimeout(context.Background(), db.timeout)
	defer cancelfunc()
	stmt, err := db.driver.PrepareContext(ctx, query)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()
	res, err := stmt.ExecContext(ctx, protocol.Date, protocol.NextcloudLink, protocol.NextcloudPath, protocol.Error, protocol.Type)
	if err != nil {
		return 0, err
	}
	_, err = res.RowsAffected()
	if err != nil {
		return 0, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	// Ensure the month exists in the database the month_with_protocol table
	month_with_year := protocol.Date.Year() * 12 + int(protocol.Date.Month()-1)
	query2 := "INSERT IGNORE month_with_protocols(month_with_year, num_protocols) VALUES (?, ?)"
	ctx2, cancelfunc2 := context.WithTimeout(context.Background(), db.timeout)
	defer cancelfunc2()
	stmt2, err := db.driver.PrepareContext(ctx2, query2)
	if err != nil {
		return 0, err
	}
	defer stmt2.Close()
	_, err = stmt2.ExecContext(ctx, month_with_year, 0)
	if err != nil {
		return 0, err
	}

	// Update the count for the month in the database
	query3 := "UPDATE month_with_protocols SET num_protocols=num_protocols+1 WHERE month_with_year=?"
	ctx3, cancelfunc3 := context.WithTimeout(context.Background(), db.timeout)
	defer cancelfunc3()
	stmt3, err := db.driver.PrepareContext(ctx3, query3)
	if err != nil {
		return 0, err
	}
	defer stmt3.Close()
	_, err = stmt3.ExecContext(ctx, month_with_year)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (db* DBClient) StoreChapter(chapter *Chapter) (int64, error) {
	query := "INSERT INTO chapter(protocol_id, headline, text, start_page, start_position, end_page, end_position) VALUES (?, ?, ?, ?, ?, ?, ?)"
	ctx, cancelfunc := context.WithTimeout(context.Background(), db.timeout)
	defer cancelfunc()
	stmt, err := db.driver.PrepareContext(ctx, query)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()
	res, err := stmt.ExecContext(ctx,
		chapter.ProtocolId, chapter.Headline, chapter.Text,
		chapter.StartPage, chapter.StartPosition,
		chapter.EndPage, chapter.EndPosition)
	if err != nil {
		return 0, err
	}
	_, err = res.RowsAffected()
	if err != nil {
		return 0, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (db* DBClient) StoreConsensus(consensus *Consensus) (int64, error) {
	query := "INSERT INTO consensus(protocol_id, chapter_id, text, start_page, start_position, end_page, end_position) VALUES (?, ?, ?, ?, ?, ?, ?)"
	ctx, cancelfunc := context.WithTimeout(context.Background(), db.timeout)
	defer cancelfunc()
	stmt, err := db.driver.PrepareContext(ctx, query)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()
	res, err := stmt.ExecContext(ctx,
		consensus.ProtocolId, consensus.ChapterId, consensus.Text,
		consensus.StartPage, consensus.StartPosition,
		consensus.EndPage, consensus.EndPosition)
	if err != nil {
		return 0, err
	}
	_, err = res.RowsAffected()
	if err != nil {
		return 0, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (db* DBClient) StoreDecision(decision *Decision) (int64, error) {
	query := "INSERT INTO decision(protocol_id, chapter_id, text, start_page, start_position, end_page, end_position) VALUES (?, ?, ?, ?, ?, ?, ?)"
	ctx, cancelfunc := context.WithTimeout(context.Background(), db.timeout)
	defer cancelfunc()
	stmt, err := db.driver.PrepareContext(ctx, query)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()
	res, err := stmt.ExecContext(ctx,
		decision.ProtocolId, decision.ChapterId, decision.Text,
		decision.StartPage, decision.StartPosition,
		decision.EndPage, decision.EndPosition)
	if err != nil {
		return 0, err
	}
	_, err = res.RowsAffected()
	if err != nil {
		return 0, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (db* DBClient) StoreProtocolError(file nextcloud_file.NextcloudFile, date time.Time, message string) {
	db.StoreProtocol(&Protocol{
		Date: date,
		Type: "butzenplenum",
		NextcloudLink: file.Link,
		NextcloudPath: file.FullPath(),
		Error: message,
	})
}

func (db* DBClient) StoreFullProtocol(file nextcloud_file.NextcloudFile, protocol *protocol_scanner.Protocol) error {
	// Store the protocol itself
	protocolId, err := db.StoreProtocol(&Protocol{
		Date: protocol.Date,
		Type: protocol.Type,
		NextcloudLink: file.Link,
		NextcloudPath: file.FullPath(),
		Error: "",
	})

	if err != nil {
		return err
	}

	// Store the chapters, creating ids
	chapterIds := make([]int64, len(protocol.Chapters))
	fmt.Println("Storing chapters", len(protocol.Chapters))
	for i, chapter := range protocol.Chapters {
		chapterId, err := db.StoreChapter(&Chapter{
			ProtocolId: protocolId,
			Headline: chapter.Headline,
			Text: chapter.Text,
			StartPage: chapter.Range.Start.Page,
			StartPosition: int(chapter.Range.Start.Position),
			EndPage: chapter.Range.End.Page,
			EndPosition: int(chapter.Range.End.Position),
		})
		if err != nil {
			return err
		}
		chapterIds[i] = chapterId
	}

	// Store the consensuses
	for _, consensus := range protocol.Consensuses {
		// Find the overlapping chapter
		chapter_id := chapterIds[0]
		for i, chapter := range protocol.Chapters {
			if protocol_scanner.IsInsideChapter(&consensus, &chapter) {
				chapter_id = chapterIds[i]
			}
		}
		_, err = db.StoreConsensus(&Consensus{
			ProtocolId: protocolId,
			ChapterId: chapter_id,
			Text: consensus.Text,
			StartPage: consensus.Range.Start.Page,
			StartPosition: int(consensus.Range.Start.Position),
			EndPage: consensus.Range.End.Page,
			EndPosition: int(consensus.Range.End.Position),
		})
		if err != nil {
			return err
		}
	}

	// Store the decisions
	for _, decision := range protocol.Decisions {
		// Find the overlapping chapter
		chapter_id := chapterIds[0]
		for i, chapter := range protocol.Chapters {
			if protocol_scanner.IsInsideChapter(&decision, &chapter) {
				chapter_id = chapterIds[i]
			}
		}
		_, err = db.StoreDecision(&Decision{
			ProtocolId: protocolId,
			ChapterId: chapter_id,
			Text: decision.Text,
			StartPage: decision.Range.Start.Page,
			StartPosition: int(decision.Range.Start.Position),
			EndPage: decision.Range.End.Page,
			EndPosition: int(decision.Range.End.Position),
		})
		if err != nil {
			return err
		}
	}

	return nil
}
