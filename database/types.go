package database

import (
	"context"
	"errors"
	"time"
)

type Protocol struct {
	Id int64
	Date time.Time
	NextcloudLink string
	NextcloudPath string
	Type string
	Error string
}

type Chapter struct {
	Id int
	ProtocolId int64
	Headline string
	Text string
	StartPage int
	StartPosition int
	EndPage int
	EndPosition int
}

type ChapterWithoutText struct {
	Id int
	ProtocolId int64
	Headline string
	StartPage int
	StartPosition int
	EndPage int
	EndPosition int
}

type Consensus struct {
	Id int
	ProtocolId int64
	ChapterId int64
	Text string
	StartPage int
	StartPosition int
	EndPage int
	EndPosition int
}

type ConsensusWithProtocolInfo struct {
	Consensus
	Date time.Time
	NextcloudLink string
}

type Decision struct {
	Id int
	ProtocolId int64
	ChapterId int64
	Text string
	StartPage int
	StartPosition int
	EndPage int
	EndPosition int
}

type DecisionWithProtocolInfo struct {
	Decision
	Date time.Time
	ProtocolId int
	NextcloudLink string
}

type MonthWithProtocols struct {
	MonthWithYear int
	NumProtocols int
}

func (mwp* MonthWithProtocols) Month() int {
	return (mwp.MonthWithYear % 12) + 1
}

func (mwp* MonthWithProtocols) Year() int {
	return mwp.MonthWithYear / 12
}

func (db* DBClient) EmptyTables() error {
	ctx, cancelFunc := context.WithTimeout(context.Background(), db.timeout)
	_, err := db.driver.ExecContext(ctx, `DELETE FROM decision`)
	if err != nil {
		return err
	}
	_, err = db.driver.ExecContext(ctx, `ALTER TABLE decision AUTO_INCREMENT = 1`)
	if err != nil {
		return err
	}
	_, err = db.driver.ExecContext(ctx, `DELETE FROM consensus`)
	if err != nil {
		return err
	}
	_, err = db.driver.ExecContext(ctx, `ALTER TABLE consensus AUTO_INCREMENT = 1`)
	if err != nil {
		return err
	}
	_, err = db.driver.ExecContext(ctx, `DELETE FROM chapter`)
	if err != nil {
		return err
	}
	_, err = db.driver.ExecContext(ctx, `ALTER TABLE chapter AUTO_INCREMENT = 1`)
	if err != nil {
		return err
	}
	_, err = db.driver.ExecContext(ctx, `DELETE FROM protocol`)
	if err != nil {
		return err
	}
	_, err = db.driver.ExecContext(ctx, `ALTER TABLE protocol AUTO_INCREMENT = 1`)
	if err != nil {
		return err
	}
	_, err = db.driver.ExecContext(ctx, `DELETE FROM month_with_protocols`)
	if err != nil {
		return err
	}
	cancelFunc()
	return nil
}

func (db* DBClient) EnsureTablesExistence() error {
	protocolTableQuery := `CREATE TABLE IF NOT EXISTS protocol(
          id int primary key auto_increment,
          date DATE NOT NULL,
          nextcloud_link VARCHAR(255) NOT NULL,
          nextcloud_path VARCHAR(255) NOT NULL,
          error VARCHAR(255) NOT NULL,
          type VARCHAR(15) NOT NULL
           )`
	chapterTableQuery := `CREATE TABLE IF NOT EXISTS chapter(
          id int primary key auto_increment,
          protocol_id int NOT NULL,
          headline TEXT NOT NULL,
          text TEXT NOT NULL,
          start_page int NOT NULL,
          start_position int NOT NULL,
          end_page int NOT NULL,
          end_position int NOT NULL,
          FULLTEXT(headline,text),
          CONSTRAINT FOREIGN KEY (protocol_id) REFERENCES protocol(id) ON UPDATE CASCADE
           )`
	consensusTableQuery := `CREATE TABLE IF NOT EXISTS consensus(
          id int primary key auto_increment,
          protocol_id int NOT NULL,
          chapter_id int NOT NULL,
          text TEXT NOT NULL,
          start_page int NOT NULL,
          start_position int NOT NULL,
          end_page int NOT NULL,
          end_position int NOT NULL,
          FULLTEXT(text),
          CONSTRAINT FOREIGN KEY (protocol_id) REFERENCES protocol(id) ON UPDATE CASCADE,
          CONSTRAINT FOREIGN KEY (chapter_id) REFERENCES chapter(id) ON UPDATE CASCADE
           )`
	decisionTableQuery := `CREATE TABLE IF NOT EXISTS decision(
          id int primary key auto_increment,
          protocol_id int NOT NULL,
          chapter_id int NOT NULL,
          text TEXT NOT NULL,
          start_page int NOT NULL,
          start_position int NOT NULL,
          end_page int NOT NULL,
          end_position int NOT NULL,
          FULLTEXT(text),
          CONSTRAINT FOREIGN KEY (protocol_id) REFERENCES protocol(id) ON UPDATE CASCADE,
          CONSTRAINT FOREIGN KEY (chapter_id) REFERENCES chapter(id) ON UPDATE CASCADE
           )`
	month_with_protocolsTableQueryQuery := `CREATE TABLE IF NOT EXISTS month_with_protocols(
          month_with_year int primary key,
          num_protocols int
           )`
	// Execute all queries
	allQueries := []string{protocolTableQuery, chapterTableQuery, consensusTableQuery, decisionTableQuery, month_with_protocolsTableQueryQuery}
	var errs []error
	for _, query := range allQueries {
		ctx, cancelFunc := context.WithTimeout(context.Background(), db.timeout)
		_, err := db.driver.ExecContext(ctx, query)
		if err != nil {
			errs = append(errs, err)
		}
		cancelFunc()
	}
	// Build a resulting error
	if len(errs) > 0 {
		errMsg := "errors: "
		for i, err := range errs {
			errMsg = errMsg + " " + err.Error()
			if i != len(errs)-1 {
				errMsg = errMsg + ", "
			}
		}
		return errors.New(errMsg)
	}
	return nil
}
