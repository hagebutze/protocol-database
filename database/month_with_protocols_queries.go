package database

import "database/sql"

func (db* DBClient) getMonthsWithProtocolsFromRows(rows *sql.Rows) ([]*MonthWithProtocols, error) {
	var result []*MonthWithProtocols

	for rows.Next() {
		var month MonthWithProtocols
		err := rows.Scan(&month.MonthWithYear, &month.NumProtocols)
		if err != nil {
			return nil, err
		}
		result = append(result, &month)
	}
	return result, nil
}

func (db* DBClient) GetMonthsWithProtocols() ([]*MonthWithProtocols, error) {
	rows, err:= db.driver.Query("SELECT month_with_year, num_protocols from month_with_protocols")

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result, err := db.getMonthsWithProtocolsFromRows(rows)
	return result, err
}
