package database

import (
	"context"
	"database/sql"
	"fmt"
	"protocol-database/config"
	"time"
)

type DBClient struct {
	driver *sql.DB
	timeout time.Duration
}

func NewClient(
	config *config.Config, timeout time.Duration,
	) (*DBClient, error) {

	connectString := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?parseTime=true",
		config.Database.Username,
		config.Database.Password,
		config.Database.Hostname,
		config.Database.Database)
	db, err := sql.Open(
		"mysql",
		connectString,
	)
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(5)
	db.SetConnMaxLifetime(timeout)
	return &DBClient{
		db,
		timeout,
	}, nil
}

func NewMockDBClient(mock *sql.DB) (*DBClient, error){
	return &DBClient{
		mock,
		time.Duration(time.Second),
	}, nil
}

func (db* DBClient) Close() {
	db.driver.Close()
}

func (db* DBClient) Ping() error {
	ctx, cancelFunc := context.WithTimeout(context.Background(), db.timeout)
	defer cancelFunc()
	err := db.driver.PingContext(ctx)
	if err != nil {
		return err
	}
	return nil
}
