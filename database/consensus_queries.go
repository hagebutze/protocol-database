package database

import "database/sql"

func (db* DBClient) GetConsensus(id int) (*Consensus, error) {
	var consensus Consensus
	err := db.driver.QueryRow("SELECT id, protocol_id, chapter_id, text, start_page, start_position, end_page, end_position from consensus WHERE id = ?", id).
		Scan(&consensus.Id, &consensus.ProtocolId, &consensus.Text, &consensus.StartPage, &consensus.StartPosition, &consensus.EndPage, &consensus.EndPosition)
	if err != nil {
		return nil, err
	}
	return &consensus, nil
}

func (db* DBClient) getConsensusesFromRows(rows *sql.Rows) ([]*Consensus, int, error) {
	var consensuses []*Consensus

	for rows.Next() {
		var consensus Consensus
		err := rows.Scan(
			&consensus.Id,
			&consensus.ProtocolId,
			&consensus.ChapterId,
			&consensus.Text,
			&consensus.StartPage,
			&consensus.EndPage,
			&consensus.StartPosition,
			&consensus.EndPosition,
		)
		if err != nil {
			return nil, 0, err
		}
		consensuses = append(consensuses, &consensus)
	}
	return consensuses, len(consensuses), nil
}

func (db* DBClient) GetAllConsensusesForProtocol(protocolId int) ([]*Consensus, error) {
	rows, err:= db.driver.Query(
		`SELECT id, protocol_id, chapter_id, text, start_page, end_page, start_position, end_position 
               FROM consensus
               WHERE protocol_id = ?`, protocolId)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	consensuses, _, err := db.getConsensusesFromRows(rows)
	return consensuses, err
}

func (db* DBClient) getConsensusesWithProtocolInfoFromRows(rows *sql.Rows) ([]*ConsensusWithProtocolInfo, int, error) {
	var consensuses []*ConsensusWithProtocolInfo

	for rows.Next() {
		var consensus ConsensusWithProtocolInfo
		err := rows.Scan(
			&consensus.Id,
			&consensus.ProtocolId,
			&consensus.ChapterId,
			&consensus.Date,
			&consensus.NextcloudLink,
			&consensus.Text,
			&consensus.StartPage,
			&consensus.EndPage,
			&consensus.StartPosition,
			&consensus.EndPosition,
		)
		if err != nil {
			return nil, 0, err
		}
		consensuses = append(consensuses, &consensus)
	}
	return consensuses, len(consensuses), nil
}

func (db* DBClient) GetAllConsensusesWithProtocolInfo() ([]*ConsensusWithProtocolInfo, int, error) {
	rows, err:= db.driver.Query(
		"SELECT c.id, protocol_id, chapter_id, date, nextcloud_link, text, start_page, end_page, start_position, end_position FROM consensus AS c INNER JOIN protocol AS p ON c.protocol_id = p.id")

	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	consensuses, num, err := db.getConsensusesWithProtocolInfoFromRows(rows)
	return consensuses, num, err
}

func (db* DBClient) GetAllConsensusesWithProtocolInfoUntil(lastDate string) ([]*ConsensusWithProtocolInfo, int, error) {
	rows, err:= db.driver.Query(
		`SELECT c.id, protocol_id, chapter_id, date, nextcloud_link, text, start_page, end_page, start_position, end_position
               FROM consensus AS c
               INNER JOIN protocol AS p
               ON c.protocol_id = p.id
               WHERE date < ?`, lastDate)

	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	consensuses, num, err := db.getConsensusesWithProtocolInfoFromRows(rows)
	return consensuses, num, err
}

func (db* DBClient) GetAllConsensusesWithProtocolInfoAfter(firstDate string) ([]*ConsensusWithProtocolInfo, int, error) {
	rows, err:= db.driver.Query(
		`SELECT c.id, protocol_id, chapter_id, date, nextcloud_link, text, start_page, end_page, start_position, end_position
			   FROM consensus AS c
			   INNER JOIN protocol AS p
			   ON c.protocol_id = p.id
			   WHERE date >= ?`, firstDate)

	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	consensuses, num, err := db.getConsensusesWithProtocolInfoFromRows(rows)
	return consensuses, num, err
}

func (db* DBClient) GetAllConsensusesWithProtocolInfoBetween(firstDate string, lastDate string) ([]*ConsensusWithProtocolInfo, int, error) {
	rows, err:= db.driver.Query(
		`SELECT c.id, protocol_id, chapter_id, date, nextcloud_link, text, start_page, end_page, start_position, end_position
               FROM consensus AS c
               INNER JOIN protocol AS p
               ON c.protocol_id = p.id
               WHERE date BETWEEN ? AND ?`, firstDate, lastDate)

	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	consensuses, num, err := db.getConsensusesWithProtocolInfoFromRows(rows)
	return consensuses, num, err
}
