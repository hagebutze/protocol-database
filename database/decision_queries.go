package database

import "database/sql"

func (db* DBClient) GetDecision(id int) (*Decision, error) {
	var decision Decision
	err := db.driver.QueryRow("SELECT id, protocol_id, chapter_id, text, start_page, start_position, end_page, end_position from decision WHERE id = ?", id).
		Scan(&decision.Id, &decision.ProtocolId, &decision.Text, &decision.StartPage, &decision.StartPosition, &decision.EndPage, &decision.EndPosition)
	if err != nil {
		return nil, err
	}
	return &decision, nil
}

func (db* DBClient) getDecisionsWithProtocolInfoFromRows(rows *sql.Rows) ([]*DecisionWithProtocolInfo, int, error) {
	var decisions []*DecisionWithProtocolInfo

	for rows.Next() {
		var decision DecisionWithProtocolInfo
		err := rows.Scan(
			&decision.Id,
			&decision.ProtocolId,
			&decision.ChapterId,
			&decision.Date,
			&decision.NextcloudLink,
			&decision.Text,
			&decision.StartPage,
			&decision.EndPage,
			&decision.StartPosition,
			&decision.EndPosition,
		)
		if err != nil {
			return nil, 0, err
		}
		decisions = append(decisions, &decision)
	}
	return decisions, len(decisions), nil
}

func (db* DBClient) getDecisionsFromRows(rows *sql.Rows) ([]*Decision, int, error) {
	var decisions []*Decision

	for rows.Next() {
		var decision Decision
		err := rows.Scan(
			&decision.Id,
			&decision.ProtocolId,
			&decision.ChapterId,
			&decision.Text,
			&decision.StartPage,
			&decision.EndPage,
			&decision.StartPosition,
			&decision.EndPosition,
		)
		if err != nil {
			return nil, 0, err
		}
		decisions = append(decisions, &decision)
	}
	return decisions, len(decisions), nil
}

func (db* DBClient) GetAllDecisionsForProtocol(protocolId int) ([]*Decision, error) {
	rows, err:= db.driver.Query(
		`SELECT id, protocol_id, chapter_id, text, start_page, end_page, start_position, end_position 
               FROM decision
               WHERE protocol_id = ?`, protocolId)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	decisions, _, err := db.getDecisionsFromRows(rows)
	return decisions, err
}

func (db* DBClient) GetAllDecisionsWithProtocolInfo() ([]*DecisionWithProtocolInfo, int, error) {
	rows, err:= db.driver.Query(
		`SELECT d.id, protocol_id, chapter_id, date, nextcloud_link, text, start_page, end_page, start_position, end_position
               FROM decision AS d
               INNER JOIN protocol AS p
               ON d.protocol_id = p.id`)

	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	decisions, num, err := db.getDecisionsWithProtocolInfoFromRows(rows)
	return decisions, num, err
}

func (db* DBClient) GetAllDecisionsWithProtocolInfoUntil(lastDate string) ([]*DecisionWithProtocolInfo, int, error) {
	rows, err:= db.driver.Query(
		`SELECT d.id, protocol_id, chapter_id, date, nextcloud_link, text, start_page, end_page, start_position, end_position
               FROM decision AS d
               INNER JOIN protocol AS p
               ON d.protocol_id = p.id
               WHERE date < ?`, lastDate)
	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	decisions, num, err := db.getDecisionsWithProtocolInfoFromRows(rows)
	return decisions, num, err
}

func (db* DBClient) GetAllDecisionsWithProtocolInfoAfter(firstDate string) ([]*DecisionWithProtocolInfo, int, error) {
	rows, err:= db.driver.Query(
		`SELECT d.id, protocol_id, chapter_id, date, nextcloud_link, text, start_page, end_page, start_position, end_position
			   FROM decision AS d
			   INNER JOIN protocol AS p
			   ON d.protocol_id = p.id
			   WHERE date >= ?`, firstDate)

	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	decisions, num, err := db.getDecisionsWithProtocolInfoFromRows(rows)
	return decisions, num, err
}

func (db* DBClient) GetAllDecisionsWithProtocolInfoBetween(firstDate string, lastDate string) ([]*DecisionWithProtocolInfo, int, error) {
	rows, err:= db.driver.Query(
		`SELECT d.id, protocol_id, chapter_id, date, nextcloud_link, text, start_page, end_page, start_position, end_position
               FROM decision AS d
               INNER JOIN protocol AS p
               ON d.protocol_id = p.id
               WHERE date BETWEEN ? AND ?`, firstDate, lastDate)

	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	decisions, num, err := db.getDecisionsWithProtocolInfoFromRows(rows)
	return decisions, num, err
}
